var gulp = require('gulp')
  , gulpIf = require('gulp-if')
  , htmlmin = require('gulp-htmlmin')
  , useref = require('gulp-useref')
  , cssnano = require('gulp-cssnano')
  , runSequence = require('run-sequence')

gulp.task('client:minify:html', function() {
  return gulp.src('client/www/*.html')
    .pipe(
	  htmlmin({
    	collapseWhitespace: true
    	, minifyCSS: true
    	, minifyJS: true
    	, removeComments: true
      })
    )
    .pipe(gulp.dest('client/www'));
});

gulp.task('client:minify:css', function() {
  return gulp.src('client/www/build/main.css')
      .pipe(cssnano())
      .pipe(gulp.dest('client/www/build/'));
});

gulp.task('admin:minify:html', function() {
  return gulp.src('admin/www/*.html')
    .pipe(
	  htmlmin({
    	collapseWhitespace: true
    	, minifyCSS: true
    	, minifyJS: true
    	, removeComments: true
      })
    )
    .pipe(gulp.dest('admin/www'));
});

gulp.task('admin:minify:css', function() {
  return gulp.src('admin/www/build/main.css')
      .pipe(cssnano())
      .pipe(gulp.dest('admin/www/build/'));
});

gulp.task('client:minify', function(cb) {
  return runSequence(['client:minify:html', 'client:minify:css'], cb)
})

gulp.task('admin:minify', function(cb) {
  return runSequence(['admin:minify:html', 'admin:minify:css'], cb)
})

gulp.task('minify', function(cb) {
  return runSequence(['client:minify', 'admin:minify'], cb)
})

gulp.task('client:build', function(cb) {
  return runSequence(['client:make', 'client:copy', 'client:minify'], cb)
})

gulp.task('admin:build', function(cb) {
  return runSequence(['admin:make', 'admin:copy', 'admin:minify'], cb)
})

gulp.task('build', function(cb) {
  return runSequence(['client:build', 'admin:build'], cb)
})

gulp.task('default', function(cb) {
  return runSequence('build', cb)
})