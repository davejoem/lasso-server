'use strict'
module.exports = (agent)=>{
  // Define our user schema
  const UserSchema = new agent.mongoose.Schema({
    accounts: {
      local: {
        phonenumber: {
          type: Number,
          unique: true,
          required: true
        }
        , username: {
          type: String,
          default: 'No Username',
          unique: false,
          required: false
        },
        activation: {
          activated: {
            type: Boolean,
            default: false,
            required: true
          }
          , activationcode: {
            type: Number,
            default: 123456,
            required: true
          }
        }
      }
      , facebook: {
        id: String,
        token: String,
        email: String,
        name: String
      }
      , twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
      }
      , google: {
        id: String,
        token: String,
        email: String,
        name: String
      }
      , instagram: {}
      , linkedin: {}
    }
    , finances: {
      balance: {
        type: Number,
        default: 0,
        required: true
      }
      , lp: {
        type: Number,
        default: 0,
        required: true
      }
      , subscription: {
        type: String,
        default: "free",
        required: true
      }
      , transactions: [
        {
          type: agent.mongoose.Schema.Types.ObjectId,
          ref: "Transaction"
        }
      ]
    }
    , wifi: {
      connections: [{
        ip: [{
          type: String,
          required: false
        }]
        , mac: [{
          type:String,
          required: true
        }]
        , profile: {
          type: String,
          unique: false,
          required: false,
          default: "one"
        }
      }]
    }
    // , Collection: {
    //   episodes: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "Episode"
    //   }],
    //   games: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "Game"
    //   }],
    //   movies: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "Movie"
    //   }],
    //   seasons: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "Season"
    //   }],
    //   shows: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "Show"
    //   }]
    // },
    // details: {
    //   title: {
    //     type: String,
    //     required: false
    //   },
    //   firstname: {
    //     type: String,
    //     minLength: 1,
    //     maxLength: 20,
    //     unique: false,
    //     required: false
    //   },
    //   middlename: {
    //     type: String,
    //     minLength: 0,
    //     maxLength: 20,
    //     unique: false,
    //     required: false
    //   },
    //   lastname: {
    //     type: String,
    //     minLength: 1,
    //     maxLength: 20,
    //     unique: false,
    //     required: false
    //   },
    //   gender: {
    //     type: String,
    //     required: false
    //   },
    //   profpic: {
    //     src: String,
    //     required: false
    //   },
    //   about: {
    //     type: String,
    //     required: false
    //   },
    //   accounttype: {
    //     type: String,
    //     required: true,
    //     default: "user"
    //   },
    //   address: {
    //     type: String,
    //     required: false
    //   },
    //   tutored: {
    //     type: Boolean,
    //     default: false
    //   },
    // },
    // devices: {
    //   maindevice: {
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     ref: "Device",
    //     required: false
    //   },
    //   signedInOn: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     ref: "Device"
    //   }],
    //   signedOutOf: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     ref: "Device"
    //   }],
    //   maxDevices: {
    //     type: Number,
    //     default: 3
    //   }
    // },
    // extras: {
    //   dedicated_ip: {}
    // },
    // interactions: {
    //   chats: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     ref: 'Chat',
    //     unique: true
    //   }],
    //   call: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     ref: 'Call',
    //     unique: true
    //   }]
    // },
    // notifications: [{
    //   type: agent.mongoose.Schema.Types.ObjectId,
    //   ref: 'Notification',
    //   unique: true
    // }],
    // settings: {
    //   apps: {
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     ref: 'Preference',
    //     required: false
    //   },
    //   general: {
    //     settingsperdevice: {
    //       type: Boolean,
    //       required: true,
    //       default: true
    //     },
    //     statesperdevice: {
    //       type: Boolean,
    //       required: true,
    //       default: true
    //     },
    //     universalpin: {
    //       type: Boolean,
    //       required: true,
    //       default: false
    //     }
    //   },
    //   social: {
    //     allowFriendRequests: {
    //       type: Boolean,
    //       required: true,
    //       default: true
    //     },
    //     allowFollows: {
    //       type: Boolean,
    //       required: true,
    //       default: true
    //     },
    //     allowProfileViewBy: {
    //       type: String,
    //       required: true,
    //       default: "all"
    //     },
    //     allowMessagesFrom: {
    //       type: String,
    //       required: true,
    //       default: "all"
    //     },
    //     notifycheckout: {
    //       type: Boolean,
    //       required: true,
    //       default: false
    //     },
    //     sharewith: {
    //       type: String,
    //       required: true,
    //       default: 'friends'
    //     },
    //     shareemail: {
    //       type: Boolean,
    //       required: true,
    //       default: true
    //     },
    //     shareimage: {
    //       type: Boolean,
    //       required: true,
    //       default: true
    //     },
    //     sharephone: {
    //       type: Boolean,
    //       required: true,
    //       default: false
    //     },
    //     sharename: {
    //       type: Boolean,
    //       required: true,
    //       default: false
    //     },
    //     shareaddress: {
    //       type: Boolean,
    //       required: true,
    //       default: false
    //     },
    //     shareabout: {
    //       type: Boolean,
    //       required: true,
    //       default: false
    //     }
    //   }
    // },
    // social: {
    //   friends: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }],
    //   requests: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }],
    //   requested: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }],
    //   followers: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }],
    //   following: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }],
    //   blocked: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }],
    //   blockers: [{
    //     type: agent.mongoose.Schema.Types.ObjectId,
    //     unique: true,
    //     ref: "User"
    //   }]
    // }
  })
  // methods ======================
  // ALL METHODS MUST BE A FUNCTION IN THE FORMAT "function(){}" AND NOT "()=>{}"

  //authentication================================================================================
  // generating a hash
  UserSchema.methods.generateHash = function(password) {
    return new Promise(resolve=>
      resolve(agent.bcrypt.hashSync(password, agent.bcrypt.genSaltSync(8), null ))
    )
  }
  // checking if password is valid
  UserSchema.methods.validPassword = function(password) {
    return new Promise(resolve=>
      resolve(agent.bcrypt.compareSync(password, this.accounts.local.password))
    )
  }
  UserSchema.methods.getId = function() {
    return new Promise(resolve=>
      resolve(this._id)
    )
  }
  UserSchema.methods.generateToken = function() {
    return new Promise(resolve=>{
      let today = new Date()
      let exp = new Date(today)
      // set expiration to 1000 days
      exp.setDate(today.getDate() + 1000)
      resolve(agent.jwt.sign({
        _id: this._id,
        exp: parseInt(exp.getTime() / 1000),
      }, agent.config.user))
    })
  }
  UserSchema.methods.getTempToken = function() {
    return new Promise(resolve=>{
      let today = new Date()
      let exp = new Date(today)
      exp.setDate(today.getDate())
      // set expiration to today
      resolve(agent.jwt.sign({
        _id: this._id,
        exp: parseInt(((exp.getTime() / 1000 / 60) + 5) * 60),
        // set expiration to today plus one minutes
      }, agent.config.user))
    })
  }
  UserSchema.methods.isActivated = function() {
    return new Promise(resolve=>resolve(this.accounts.local.activation.activated))
  }
  UserSchema.methods.activate = function(code) {
    return new Promise((resolve,reject)=>{
      if (this.accounts.local.activation.activationcode == code) {
        this.accounts.local.activation.activated = true
        resolve(this)
      } else
        reject(new Error("Invalid code."))
    })
  }
  UserSchema.methods.generateActivationCode = function() {
    return new Promise(resolve=>{
      let code = ''
      while (code.length < 6) {
        let digit = Math.ceil(Math.random()*9)
        code += digit
      }
      this.accounts.local.activation.activationcode = parseInt(code)
      this.accounts.local.activation.activated = false
      resolve(this)
    })
  }
  UserSchema.methods.sendActivationCode = function(method) {
    return new Promise((resolve, reject)=>{
      this.generateActivationCode().then(
        user=>{
          let sendByPhone = ()=>{
            let url =  `${agent.config.sms.api}/send?device=${agent.config.sms.device}&email=${agent.config.sms.email}&password=${agent.config.sms.password}&message=Welcome to Lasso. Your activation code is ${user.accounts.local.activation.activationcode}.&number=+254${user.accounts.local.phonenumber}`
            agent.request.get({
              headers: {
                'Accepts': 'application/json'
              },
              url: url
            }).on('response', response=>{
              if (response.statusCode == 200) {
		dontSend()
                resolve(this)
	      } else {
		dontSend()
                reject(response.statusCode)
	      }
            })
            .on('error', error=>reject(error))
          }
          , dontSend = ()=>{ 
              process.stdout.write(`Activation code: ${this.accounts.local.activation.activationcode}\n`)
              resolve(this)
          }

          switch (method) {
            case 'phone':
              sendByPhone()
              break
            case 'none':
              dontSend()
              break
            default:
              sendByPhone()
          }
        }
      )
    })
  }
  UserSchema.methods.generateUserSpace = function() {
    return new Promise((resolve, reject)=>{
      process.stdout.write(`creating a space for user: ${this.accounts.local.phonenumber} \n`)
      agent.fs.ensureFile("./fs/users/" + this.accounts.local.phonenumber + "/logs/log.txt", (err)=>{
        if (err) {
          reject(err)
          return
        }
        resolve()
      })
    })
  }
  UserSchema.methods.deleteUser = function(phonenumber) {
    new Promise((resolve, reject)=>{
      process.stdout.write(`Deleting user and space for user: ${this.accounts.local.phonenumber} \n`)
      agent.fs.stat('./fs/users/' + phonenumber, (err,stats)=>{
        if (err) {
           reject(err)
          return
        }
        if (stats) {
          agent.fs.unlink("./fs/users/" + phonenumber, (err)=>{
            if (err) {
              reject(err)
              return
            }
            resolve(null , true)
          }
          )
        }
        let arr = this.social.friends.concat(this.social.followers).concat(this.social.following).concat(this.social.requested).concat(this.social.requests).concat(this.social.blocked)
        arr.forEach((member)=>{
          agent.models.User.findById(member, (err,user)=>{
            if (err) {
              reject(err)
              return
            }
            if (user.social.friends.indexOf(this._id))
              user.social.friends.pull(this._id)
            if (user.social.followers.indexOf(this._id))
              user.social.followers.pull(this._id)
            if (user.social.following.indexOf(this._id))
              user.social.following.pull(this._id)
            if (user.social.requested.indexOf(this._id))
              user.social.requested.pull(this._id)
            if (user.social.requests.indexOf(this._id))
              user.social.requests.pull(this._id)
            if (user.social.blockers.indexOf(this._id))
              user.social.blockers.pull(this._id)
            user.save()
          }
          )
        }
        )
      }
      )
    })
  }
  //runtime====================================================================================
  UserSchema.methods.startCredit = function(amount, mac) {
    return new Promise(resolve=>{
      let minus = ()=>{
        if ((this.finances.balance + this.finances.lp) > amount) {
          if (this.finances.balance > amount) this.finances.balance -= amount
          else this.finances.lp -= amount
          this.save()
        } else {
          process.stdout.write(`Logging out user ${mac} due to insufficient balance`)
          this.logoutDevice(mac).then(()=>{
            process.stdout.write(`User ${mac} logged out.\n`)
          }).catch(()=>{
            process.stdout.write(`Couldn't logout user ${mac}.\n`)
          })
        }
      }
      let int = setInterval(minus, 60000)
      agent.router.connections[mac] = int
      resolve(this, int)
    })
  }
  UserSchema.methods.creditOnce = function(amount, creditfrom) {
    process.stdout.write(`Crediting ${this.accounts.local.phonenumber} ${amount}`)
    let self = this
    return new Promise((resolve, reject)=>{
      switch(creditfrom) {
        case 'balance':
          if (amount > self.finances.balance)
            reject("Insufficient balance")
          else {
            self.finances.balance -= amount
            resolve(self)
          }
          break
        case 'lp':
          if (amount > self.finances.lp)
            reject("Insufficient LP")
          else {
            self.finances.lp -= amount
            resolve(self)
          }
          break
        default:
          if (amount > (self.finances.balance + self.finances.lp))
            reject("Insufficient balance + LP")
          else {
            let m = amount
            while (m > 0) {
              m--
              if (self.finances.balance > 0) self.finances.balance--
              else self.finances.lp--
              console.log(m,self.finances.balance,self.finances.lp)
            }
            resolve(self)
          }
          break;
      }
    })
  }
  UserSchema.methods.logoutDevice = function(mac) {
    return new Promise((resolve, reject)=>{
      agent.router.conn.getCommandPromise([
        `/ip/hotspot/user/print`
        , `?name=${mac}`
      ]).then(users=> {
        agent.router.conn.getCommandPromise([
          '/ip/hotspot/user/remove'
          , `=numbers=${parseInt(users[0][`.id`].charAt(1))}`
        ]).then(()=>{
          agent.router.conn.getCommandPromise([
            '/ip/hotspot/active/remove'
            , `=numbers=${parseInt(users[0][`.id`].charAt(1))}`
          ]).then(()=>{
            clearInterval(agent.router.connections[mac])
            agent.router.connections.splice(agent.router.connections.indexOf(mac), 1)
            resolve()
          }).catch(reject)
        }).catch(reject)
      }).catch(reject)
    })
  }
  UserSchema.methods.canSignin = function() {
    return {
      canSignin: this.details.accounttype == 'admin' || this.finances.subscription != 'free' ? true : this.devices.signedInOn.length < this.devices.maxDevices,
      signedInOn: this.devices.signedInOn,
      signedInOnLength: this.devices.signedInOn.length,
      maxDevices: this.devices.maxDevices,
    }
  }
  UserSchema.methods.signInOnClientDevice = function(deviceid, cb) {

  }
  UserSchema.methods.signOutOfClientDevice = function(deviceid, cb) {

  }
  UserSchema.methods.setMainDevice = function(deviceid, cb) {
    process.stdout.write(`signing in on device ${deviceid}\n`)
    this.devices.maindevice = deviceid
    this.devices.signedOutOf.pull(deviceid)
    if (this.devices.signedInOn.indexOf(deviceid) == -1) {
      this.devices.signedInOn.push(deviceid)
    }
    agent.models.Device.findById(deviceid, (err,device)=>{
      if (err)
        return cb(err)
      if (device) {
        device.signInUser(this._id, (err,device)=>{
          if (err)
            return cb(err)
          device.save((err,device)=>{
            if (err)
              return cb(err)
            this.save((err)=>{
              if (err) return cb(err)
              cb(null , this)
            })
          }
          )
        }
        )
      }
    }
    )
  }
  UserSchema.methods.unSetMainDevice = function(deviceid, cb) {
    this.accounts.local.activation.activated = false
    this.devices.signedInOn.pull(deviceid)
    if (this.devices.signedOutOf.indexOf(deviceid) == -1)
      this.devices.signedOutOf.push(deviceid)
    agent.models.Device.findById(deviceid, (err,device)=>{
      if (err)
        return cb(err)
      if (device) {
        device.signOutUser(this._id, (err,device)=>{
          if (err)
            return cb(err)
          device.save((err,device)=>{
            if (err) return cb(err)
            this.save((err)=>{
              if (err) return cb(err)
              cb(null, this)
            })
          }
          )
        }
        )
      }
    }
    )
  }
  UserSchema.methods.notify = function(config, cb) {
    if (config.devices == 'all') {
      config.devices = this.devices.signedInOn
    }
    if (config.devices == 'allbutthis') {
      config.devices = this.devices.signedInOn
      config.devices.splice(this.devices.signedInOn.indexOf(config.device), 1)
    }
    var note = new agent.models.Notification(config)
    note.user = this._id
    note.save((err,note)=>{
      if (err && cb)
        return cb(err)
      this.notifications.push(note._id)
      this.save((err,user)=>{
        if (err)
          return cb(err)
        cb(null , {
            user: user,
            notification: note
          })
      }
      )
    }
    )
  }
  UserSchema.methods.renotify = function(notification, cb) {
    notification.read = false
    notification.save((err,note)=>{
      if (err && cb)
        return cb(err)
      if (this.notifications.unread.indexOf(note._id) < 0) {
        this.notifications.unread.push(note._id)
      }
      this.save((err,user)=>{
        if (err)
          return cb(err)
        cb(null , {
          user: user,
          notification: note
        })
      }
      )
    }
    )
  }
  // expose schema to our app
  // return agent.database.db.model("User", UserSchema)
  return agent.mongoose.model("User", UserSchema)
}