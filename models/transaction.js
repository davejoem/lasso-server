'use strict'
module.exports = (agent)=>{
  const TransactionSchema = new agent.mongoose.Schema({
    amount: {
      type: Number,
      required: true,
      default: 0
    }
    , carrier: {
      type: String,
      required: true
    }
    , code: {
      type: String,
      required: true,
      unique: true
    }
    , date: {
      type: Date,
      required: true
    }
    , sender: {
      name: {
        type: String,
        required: true
      }
      , number: {
        type: Number,
        required: true
      }
    }
    , user: {
      type: agent.mongoose.Schema.Types.ObjectId
      , ref: "User"
      , required: false
    }
    , used: {
      type: Boolean
      , required: true
      , default: false
    }
  })
  // methods ======================
  // ALL METHODS MUST BE A FUNCTION IN THE FORMAT "function(){}" AND NOT "()=>{}"

  TransactionSchema.methods.own = function() {

  }

  // expose schema to our app
 // return agent.database.db.model("Transaction", TransactionSchema)
 return agent.mongoose.model("Transaction", TransactionSchema)
}