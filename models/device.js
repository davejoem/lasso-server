'use strict'
module.exports = (agent)=>{
  // Define a device schema
  const DeviceSchema = new agent.mongoose.Schema({
    firstLogin: Date,
    id: {
      type: String,
      required: true
    },
    lastOnline: Date,
    location: {
      type: String,
      required: false
    },
    machine: {
      type: String,
      required: false
    },
    name :  {
      type: String,
      required: false
    },
    os: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: false
    },
    users: {
      current: {
        type: agent.mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: false
      },
      signedOut: [{
        type: agent.mongoose.Schema.Types.ObjectId,
        ref: "User"
      }]
    }
  })
  
  // methods ======================
  DeviceSchema.methods.generateToken = function(cb) {
    var token = agent.jwt.sign({
      id : this._id    
    }, agent.config.device);
    this.token = token
    if (cb) return cb(null, token);
    return token;
  }
  DeviceSchema.methods.signInUser = function(userid,cb) {
    this.users.signedOut.push(this.users.current)
    this.users.current = userid
    cb(null, this)
  }
  DeviceSchema.methods.signOutUser = function(userid,cb) {
    this.users.current = null
    this.users.signedOut.push(userid)
    cb(null, this)
  }
  // methods ======================
  // ALL METHODS MUST BE A FUNCTION IN THE FORMAT "function(){}" AND NOT "()=>{}"
  
  // return agent.database.db.model("Device", DeviceSchema)
  return agent.mongoose.model("Device", DeviceSchema)
}