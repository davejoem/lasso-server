'use strict'

module.exports = (agent) => {
  const MovieSchema = new agent.mongoose.Schema({
    actors: String,
    awards: String,
    adult: {type: Boolean, default: false, required: true},
    availableextensions: [{type: String}],
    averagerating: {type: Number, default: 0, required: true},
    backdrop_path: String,
    belongs_to_collection:{
      id: Number,
      name: String,
      poster_path: String,
      backdrop_path: String
    },
    boxoffice: String,
    budget: Number,
    comments: [{
        author: {
          id: {type: agent.mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
          username: {type: String, required: true},
        },
        comment: {type: String, required: true},
        rating: {type: Number, default: 0, required: true},
        likes: {type: Number, default: 0, required: true},
        dislikes: {type: Number, default: 0, required: true},
        date: {type: Date, default: Date.now(), required: true}
    }],
    id: Number,
    country: String,
    director: String,
    downloads: {type: Number, default: 0, required: true},
    dvd: String,
    genre: String,
    genres: [{id: Number, name: String}],
    homepage: String,
    imdb_id: {type: String, required: false, unique: true},
    imdbid: {type: String, required: false, unique: true},
    imdbrating: String,
    imdbvotes: String,
    language: String,
    metascore: String,
    original_language: String,
    original_title: String,
    overview:  String,
    owners: [{ type: agent.mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
    path: {type: String, required: true, unique: true},
    plays: {type: Number, default: 0, required: true},
    plot: {type: String, required: false},
    popularity: Number,
    poster: String,
    Poster: String,
    poster_path: String,
    price: {type: Number, default: 30, required: true},
    production: String,
    production_companies: [{name: String, id: Number}],
    production_countries: [{iso_3166_1: String, name: String}],
    rated: {type: String, required: true},
    released: {type: String, required: true},
    release_date: String,
    revenue: Number,
    runtime: {type: String, required: true},
    spoken_languages: [{iso_639_1: String, name: String}],
    sharers: [{ type: agent.mongoose.Schema.Types.ObjectId, ref: 'User', unique: true }],
    sharedwiths: [{ type: agent.mongoose.Schema.Types.ObjectId, ref: 'User', unique: true }],
    status: String,
    tagline: String,
    title: {type: String, required: true},
    tomatoconsensus: String,
    tomatofresh: String,
    tomatoimage: String,
    tomatometer: String,
    tomatorating: String,
    tomatoreviews: String,
    tomatorotten: String,
    tomatourl: String,
    tomatousermeter: String,
    tomatouserrating: String,
    tomatouserreviews: String,
    type: {type: String, required: true, default:"movie"},
    video: Boolean,
    vote_average: Number,
    vote_count: Number,
    website: String,
    writer: String,
    year: String
  });
  // methods ======================
  // adding a comment
  MovieSchema.methods.addComment = function(comment,cb) {
    return new Promise(resolve=>{
      this.comments.push(comment);
      resolve(this)
    })
  }
  // like a comment
  MovieSchema.methods.like = function(index,cb) {
    return new Promise(resolve=>{
      this.comments[index].likes++;
      resolve(this)
    })
  }
  // dislike a comment
  MovieSchema.methods.dislike = function(index) {
    return new Promise(resolve=>{
      this.comments[index].dislikes++;
      resolve(this)
    })
  }
  // calculate new rating
  MovieSchema.methods.calcAverageRating = function() {
    return new Promise((resolve)=>{
      var total=0, averagerating=0;
      for(let i=0; i<this.comments.length; i++){
        total = this.comments[i].rating+total;
        averagerating = total/this.comments.length;
      }
      Math.round(averagerating);
      this.averagerating = averagerating;
      resolve(this)
    })
  }
  // create the model for movies and expose it to our app
  // return agent.database.db.model('Movie', MovieSchema);
  return agent.mongoose.model('Movie', MovieSchema);
}