webpackJsonp([0],{

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__users__ = __webpack_require__(758);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UsersModule = (function () {
    function UsersModule() {
    }
    return UsersModule;
}());
UsersModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__users__["a" /* Users */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__users__["a" /* Users */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__users__["a" /* Users */]
        ]
    })
], UsersModule);

//# sourceMappingURL=users.module.js.map

/***/ }),

/***/ 758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Users; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(414);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Users = (function () {
    function Users(api, user, modalCtrl, navCtrl, navParams) {
        this.api = api;
        this.user = user;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Users.prototype.ngOnInit = function () {
        var _this = this;
        setInterval(function () {
            _this.api.post('/admin/users', { token: _this.user._user })
                .map(function (res) { return res.json(); })
                .subscribe(function (res) { return _this.users = res.users; }, function (err) { return console.log(err); });
        }, 10000);
    };
    Users.prototype.ionViewDidLoad = function () { };
    Users.prototype.viewUser = function (user) {
        console.log(user);
        var modal = this.modalCtrl.create("User", { user: user }, {
            enableBackdropDismiss: true
        });
        modal.present();
    };
    return Users;
}());
Users = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'users',template:/*ion-inline-start:"/home/ubuntu/workspace/admin/src/pages/users/users.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>Users</ion-title>\n    <button ion-button icon-only menuToggle="right" right>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-segment [(ngModel)]="category" color="primary">\n    <ion-segment-button value="active">\n      <ion-icon name="pulse"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button value="inactive">\n      <ion-icon name="eye-off"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button value="previous">\n      <ion-icon name="bookmark"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button value="disabled">\n      <ion-icon name="bookmark"></ion-icon>\n    </ion-segment-button>\n  </ion-segment>\n  <ion-list>\n    <ion-item *ngFor="let user of users">\n      <ion-thumbnail item-start>\n        <img src="/assets/imgs/lasso.png">\n      </ion-thumbnail>\n      <p>ID: {{user[\'.id\']}}</p>\n      <p>Name: {{user.name}}</p>\n      <p>Uptime: {{user.uptime}}</p>\n      <p>Bytes In {{user.bytes-in}}</p>\n      <p>Bytes Out: {{user.bytes-out}}</p>\n      <p>Packets In: {{user.packets-in}}</p>\n      <p>Packets Out: {{user.packets-out}}</p>\n      <p>Disabled: {{user.disabled}}</p>\n      <p>Dynamic: {{user.dynamic}}</p>\n      <button ion-button clear item-end (click)="viewUser(user)">Details</button>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/ubuntu/workspace/admin/src/pages/users/users.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_providers__["a" /* Api */],
        __WEBPACK_IMPORTED_MODULE_2__providers_providers__["b" /* User */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], Users);

//# sourceMappingURL=users.js.map

/***/ })

});
//# sourceMappingURL=0.main.js.map