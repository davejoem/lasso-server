webpackJsonp([2],{

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusModule", function() { return StatusModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__status__ = __webpack_require__(756);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StatusModule = (function () {
    function StatusModule() {
    }
    return StatusModule;
}());
StatusModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__status__["a" /* Status */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__status__["a" /* Status */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__status__["a" /* Status */]
        ]
    })
], StatusModule);

//# sourceMappingURL=status.module.js.map

/***/ }),

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Status; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(414);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Status = (function () {
    function Status(menuCtrl, navParams, user) {
        this.menuCtrl = menuCtrl;
        this.navParams = navParams;
        this.user = user;
        this.active_tab = 0;
    }
    Status.prototype.ngOnInit = function () {
        this.tab1Root = 'Users';
        this.tab2Root = 'Payments';
        this.tab3Root = 'Metrics';
    };
    Status.prototype.ionViewWillEnter = function () {
        if (this.user._user == null || this.user._user == 'undefined')
            return location.assign('https://lasso-server.herokuapp.com/admin');
        if (this.navParams.get('index') == null || this.navParams.get('index') == "undefined")
            this.active_tab = 0;
        else
            this.active_tab = this.navParams.get('index');
    };
    Status.prototype.ionViewDidEnter = function () {
        this.menuCtrl.enable(true);
        this.menuCtrl.open();
        this.tabs.select(this.active_tab);
    };
    return Status;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('statusTabs'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Tabs */])
], Status.prototype, "tabs", void 0);
Status = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'status',template:/*ion-inline-start:"/home/ubuntu/workspace/admin/src/pages/status/status.html"*/'<ion-tabs  #statusTabs>\n  <ion-tab [root]="tab1Root" tabIcon="people">Users</ion-tab>\n  <ion-tab [root]="tab2Root" tabIcon="cash">Money</ion-tab>\n  <ion-tab [root]="tab3Root" tabIcon="analytics">Metrics</ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/ubuntu/workspace/admin/src/pages/status/status.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_providers__["b" /* User */]])
], Status);

//# sourceMappingURL=status.js.map

/***/ })

});
//# sourceMappingURL=2.main.js.map