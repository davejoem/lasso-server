import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage, IonicStorageModule } from '@ionic/storage';

import { LassoAdmin } from './app.component';

// import { Bundle } from '../components/bundle/bundle'
import { PipesModule } from '../pipes/pipes.module';

import { Api } from '../providers/api';
import { User } from '../providers/user';

// import { Camera } from '@ionic-native/camera';
// import { GoogleMaps } from '@ionic-native/google-maps';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { StatusBar } from '@ionic-native/status-bar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  LassoAdmin
]

export function declarations() {
  return pages
}

export function entryComponents() {
  return pages
}

export function providers() {
  return [
    Api
    , User
    
    // , Camera
    // , GoogleMaps
    // , SplashScreen
    // , StatusBar,

    // Keep this to enable Ionic's runtime error handling during development
    , { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations()
  , imports: [
    BrowserModule
    , HttpModule
    , PipesModule
    , TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader
        , useFactory: HttpLoaderFactory
        , deps: [Http]
      }
    })
    , IonicModule.forRoot(LassoAdmin)
    , IonicStorageModule.forRoot()
  ]
  , bootstrap: [IonicApp]
  , entryComponents: entryComponents()
  , providers: providers()
})
export class AppModule { }
