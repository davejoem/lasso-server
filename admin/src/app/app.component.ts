import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Alert, AlertController, Config, Loading, LoadingController, MenuController, Nav, Platform } from 'ionic-angular';
import { Observable } from 'rxjs'
import 'rxjs/add/operator/retry'
// import { SplashScreen } from '@ionic-native/splash-screen';

import { Api, User } from '../providers/providers'

import { TranslateService } from '@ngx-translate/core'

@Component({
  templateUrl: `app.html`
})
export class LassoAdmin implements OnDestroy, OnInit {
  @ViewChild(Nav) nav: Nav;
  public loading: Loading
  public menu: {
    side: string
    type: string
  }
  public rootPage: any
  public rootParams: any
  public sign_alert: Alert
  public welcome_alert: Alert

  constructor(
    private alertCtrl: AlertController
    , private api: Api
    , private config: Config
    , private loadingCtrl: LoadingController
    , private menuCtrl: MenuController
    , private translate: TranslateService
    , private platform: Platform
    , private user: User
    // , statusBar: StatusBar
    // , splashScreen: SplashScreen
  ) {
    this.initTranslate()
    this.menu = {
      side: `right`
      , type: `overlay`
    }
  }
  ngOnInit() {
    this.platform.ready().then(() => {
      this.menuCtrl.enable(false)
      this.user.load().then(
        user => {
          if (user != null) {
            this.user._signedIn(user)
            this.showWelcomeAlert(true)
          }
          else
            this.showWelcomeAlert(false)
        }
        , () => this.showWelcomeAlert(false)
      )
    })
  }
  ngOnDestroy() {
    this.api.disconnectSocket()
  }
  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  showErrorAlert(error: any, dismissable?: any) {
    let alert: Alert = this.alertCtrl.create({
      title: `<h5 class="danger">Error</h5>`
      , message: error.message ? error.message : error
      , buttons: [
        {
          text: `Ok`
          , handler: () => {
            alert.dismiss()
            return false
          }
        }
      ]
    })
    dismissable != null
      ? dismissable.dismiss().then(() => alert.present())
      : alert.present()
  }

  showSignAlert(error: boolean, message?: string): Alert {
    this.sign_alert = this.alertCtrl.create({
      title: '<p class="lasso">Sign In</p>'
      , subTitle: 'Enter details to sign in to router'
      , message: error ? `<p class="danger">${message ? message : 'An error occured. Please check your internet connection and details entered then try again.'}</p>` : null
      , inputs: [
        {
          name: 'ip',
          placeholder: 'IP Address',
          type: 'string'
        }
        , {
          name: 'username',
          placeholder: 'Username',
          type: 'string'
        }
        , {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ]
      , buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.sign_alert.dismiss().then(() => { this.showWelcomeAlert(false) })
            return false
          }
        },
        {
          text: 'Sign In',
          handler: data => {
            this.sign_alert.dismiss().then(() => {
              this.loading = this.loadingCtrl.create({
                content: `Connecting ...`
              })
              this.loading.present().then(() =>
                this.user.signIn(data).subscribe(
                  res => {
                    this.loading.dismiss().then(() => {
                      this.loading = this.loadingCtrl.create({
                        content: `Saving ...`
                      })
                      this.loading.present().then(() =>
                        this.user.save(res.token).then(
                          () => this.loading.dismiss().then(
                            () => {
                              this.rootPage = 'Status'
                              this.rootParams = { index: 0 }
                            }
                          )
                          , err => this.loading.dismiss().then(() => this.showSignAlert(true))
                        )
                      )
                    })
                  }
                  , err => this.showSignAlert(true, err.message)
                )
              )
            })
            return false
          }
        }
      ]
      , enableBackdropDismiss: false
    })
    this.sign_alert.present()
    return this.sign_alert
  }

  showWelcomeAlert(has_user: boolean) {
    this.loading = this.loadingCtrl.create({
      content: `Please wait ...`
    })
    this.loading.present().then(
      () => this.api.post('/admin/loggedin', { token: has_user ? this.user._user : "pre" }).subscribe(
        res => {
          this.welcome_alert = this.alertCtrl.create({
            title: '<p class="lasso">Lasso Network Management Portal</p>'
            , subTitle: 'Welcome'
            , message: `
              <p>This portal helps you manage the network, view various status-updates and much more.</p>
              <p><b>Click ${
            res.loggedin
              ? has_user
                ? 'Continue'
                : 'Sign In'
              : 'Sign In'
            } to proceed.</b></p>`
            , buttons: [
              {
                text: res.loggedin
                  ? has_user
                    ? 'Continue'
                    : 'Sign In'
                  : 'Sign In'
                , handler: () => {
                  this.welcome_alert.dismiss().then(() => {
                    res.loggedin
                      ? has_user
                        ? this.rootPage = `Status`
                        : this.showSignAlert(false)
                      : this.showSignAlert(false)
                  })
                  return false
                }
              }
            ]
            , enableBackdropDismiss: false
          })
          this.loading.dismiss().then(() => this.welcome_alert.present())
        }
        , err => {
          if (err.status == `Unverified`) {
            return this.user.signOut().then(() =>
              this.loading.dismiss().then(() =>
                this.showWelcomeAlert(false)
              )
            )
          }
          if (err.status == `failed`) {
            return this.loading.dismiss().then(() => this.showWelcomeAlert(false))
          }
          this.loading.dismiss().then(() => this.showErrorAlert(`Couldn't check if router is logged in!`))
        }
      )
    )
  }

  signOut() {
    let signingOut = this.loadingCtrl.create({ content: `Signing out ...` })
    signingOut.present().then(() =>
      this.api.post('/admin/logout/', { token: this.user._user }).subscribe(
        res => {
          this.user.signOut().then(
            () => signingOut.dismiss().then(() => {
              let signedOut = this.alertCtrl.create({
                title: `<p class="lasso">${res.title}</p>`
                , message: `<p>${res.message}</p>`
                , buttons: [
                  {
                    text: 'Ok'
                    , handler: () => {
                      signedOut.dismiss().then(() => this.showWelcomeAlert(false))
                      return false
                    }
                  }
                ]
                , enableBackdropDismiss: false
              })
              signedOut.present()
            })
            , console.error
          )
        }
        , console.error
      )
    )
  }
  view(page: number): void {
    this.nav.setRoot(`Status`, { index: page })
  }
}
