import { NgModule } from '@angular/core';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [],
  imports: [
    MomentModule    
  ],
  exports: [
    MomentModule
  ]
})
export class PipesModule { }