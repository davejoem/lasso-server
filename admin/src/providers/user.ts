import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Api } from './api';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  private USER_KEY: string = 'token';
  public _user: any;

  constructor(
    public api: Api
    , public storage: Storage
  ) {}
  
  
  /**
   * Send a POST request to our code generation endpoint with the data
   * the user entered on the form.
   */
  genToken(username: any) {
    return this.api.post(`/admin/token`, username)
  }
  
  /**
   * Fetch user from storage
   */
  load() {
    return this.storage.get(this.USER_KEY)
  }
  
  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  signIn(details: any) {
    return this.api.post(`/admin/login`, details)
  }
  
  /**
   * Sign the user out, which forgets the session
   */
  signOut() {
    this._user = null;
    return this.save()
  }

  /**
   * Process a signin/signup response to store user data
   */
  _signedIn(user) {
    this._user = user;
  }
  save(user?: any) {
    return this.storage.set(this.USER_KEY, user ? user : this._user)
  }
}
