import { Component, OnInit } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';

import { Api , User } from '../../providers/providers'

@IonicPage()
@Component({
  selector: 'users',
  templateUrl: 'users.html',
})
export class Users implements OnInit {
  public category: string
  public users: any
  constructor(
    private api: Api
    , private user: User
    , private modalCtrl: ModalController
    , private navCtrl: NavController
    , private navParams: NavParams
  ) {}
  ngOnInit() {
    setInterval(()=>{
      this.api.post('/admin/users', { token: this.user._user })
        .map(res => res.json())
        .subscribe(
          res=>this.users = res.users
          , err=>console.log(err)
        )
    }, 10000)
  }
  ionViewDidLoad() {}
  viewUser(user): void {
    console.log(user)
    let modal = this.modalCtrl.create(`User`, { user: user }, {
      enableBackdropDismiss: true
    })
    modal.present()
  }
}
