import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, MenuController, NavParams, Tabs } from 'ionic-angular';

import { User } from '../../providers/providers'

@IonicPage()
@Component({
  selector: 'status',
  templateUrl: 'status.html'
})
export class Status implements OnInit {
  @ViewChild('statusTabs') tabs: Tabs
  public active_tab: number
  public tab1Root: any
  public tab2Root: any
  public tab3Root: any
  constructor(
    private menuCtrl: MenuController
    , private navParams: NavParams
    , private user:User
  ) {
    this.active_tab = 0
  }
  
  ngOnInit() {
    this.tab1Root = 'Users'
    this.tab2Root = 'Payments'
    this.tab3Root = 'Metrics'
  }
  ionViewWillEnter() {
    if (this.user._user == null || this.user._user == 'undefined')
      return location.assign('https://lasso-server.herokuapp.com/admin')
    if (this.navParams.get('index') == null || this.navParams.get('index') == `undefined`)
      this.active_tab = 0
    else
      this.active_tab = this.navParams.get('index')
  }
  ionViewDidEnter() {
    this.menuCtrl.enable(true)
    this.menuCtrl.open();
    this.tabs.select(this.active_tab)
  }
}