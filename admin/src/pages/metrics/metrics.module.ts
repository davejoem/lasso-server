import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Metrics } from './metrics';

@NgModule({
  declarations: [
    Metrics,
  ],
  imports: [
    IonicPageModule.forChild(Metrics),
  ],
  exports: [
    Metrics
  ]
})
export class MetricsModule {}
