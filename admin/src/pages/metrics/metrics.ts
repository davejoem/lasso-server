import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MetricsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'metrics',
  templateUrl: 'metrics.html',
})
export class Metrics {

  constructor(
    public navCtrl: NavController
    , public navParams: NavParams
  ) {}

  ionViewDidLoad() {
    
  }

}
