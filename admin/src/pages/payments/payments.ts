import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Api, User } from '../../providers/providers'
@IonicPage()
@Component({
  selector: 'payments',
  templateUrl: 'payments.html',
})
export class Payments {
  public payments: any[]
  constructor(
    private api: Api  
    , public navCtrl: NavController
    , public navParams: NavParams
    , private user: User
  ) {}

  ionViewDidLoad() {
    this.api.post('/admin/payments', { token: this.user._user })
      .map(res=>res.json())
      .subscribe(
        res=>this.payments = res.payments
        , err=>{
          console.error(err)
        }
      )
  }

}
