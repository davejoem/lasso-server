'use strict'

module.exports = (agent) => {
  return class {
    constructor() {
      this.api = agent.config.omdbAPI
      this.key = agent.config.auth.omdb
    }
    init() {
      return new Promise((resolve,reject)=>{
        resolve()
      })
    }
    obtain(req) {
      return new Promise((resolve,reject) => {
        let responseData = "";
        if (req) {
          agent.https.get(req, (res) => {
            res.on('data', chunk=>responseData += chunk)
            .on('error', err=>{
              agent.utils.log({log: "OMDB-GET: Response stream error!", err: err})
              reject(err)
            })
            .on('end',
              ()=>{
                let obj = JSON.parse(responseData)
                if (obj.Response == "True") {
                  agent.utils.log({log: "OMDB-GET: Response success.", success: "Item found"})
                  return resolve(agent.utils.fn.objectKeysToLowerCase(obj))                                        
                }
                if (obj.Response == "False") {
                  let err = new Error("Item not found")
                  agent.utils.log({log: "OMDB-GET: " + path + " returned zero results!", err: err})
                  return reject(err)
                }
              }
            )
          })
          .on('error', err=>{
            agent.utils.log({log: "OMDB-GET: Web request error!", err: err})
            reject(err)
          })
        }   
        else {
          let err = new Error("Expects first argument to be an object or a string.")
          agent.utils.log({log: "OMDB-GET: Request object/path error!", err: err})
          reject(err)
        }
      })
    }
    search(query) {
      return new Promise((resolve,reject) => {
        if (query) {
          switch (typeof query) {
            case 'object':
              /*expect a query to be an object in the form of:
                * query = {
                *    scheme: 't=' || 'i=' || 's=', -- required
                *    search: 'String' -- required
                *    year: 'String' --optional
                *    type: 'String' --optional
                *    year: 'String' --optional
                *    r: 'String'    --optional
                *    plot: 'String' --optional   //only when query.scheme = "t=" or "i="
                *    page: 'string' --optional  //only when query.scheme = "s="
                *    tomatoes: String --optional      //only when query.scheme = "t=" or "i="
                * } 
              */
              if (!query.scheme || !query.search) {
                let err = new Error("Invalid search parameters.");
                agent.utils.log({log: "Invalid search parameters.", err: err});
                reject(err);
              }
              var path = this.api +  query.scheme + query.search;
              if (query.type == "movie") path += "&type=" + query.type;
              if (query.year) path += "&y=" + query.year;
              if (query.plot) {
                if (query.scheme == "t=" || query.scheme == "i=") path += "&plot=" + query.plot
              }
              if ((query.scheme == "t=" || query.scheme == "i=") && !query.plot) path += "&plot=full"
              if (query.scheme == "s=" && query.page) path += "&page" + query.page
              if (query.r) path += "&r" + query.r 
              if (!query.r) path += "&r=json"
              if (query.tomatoes) path += "&tomatoes=true"
              this.obtain(path).then(
                (res) => resolve(res)
                , (err) => reject(err)                        
              )
              break
            case 'string':
              this.obtain(path).then(
                res=>resolve(res)
                , err=>reject(err)
              )
          } 
        } else {
          let err = new Error("Expects first argument to be an object or a string.")
          agent.utils.log({log: "[OMDB] Request object/path error!", err: err})
          reject(err)
        }            
      })
    }
  }
}