module.exports = (agent) => {
    var consts  = agent.consts;
    var fs      = agent.fs;
    var path    = agent.path;
    return {
        log: (err,cb) => {    
            fs.appendFile(path.resolve(consts.userLogFile), err.log, (err, log) => {
                if (err) return cb(err, null);
                return cb(null, log);
            });
        }
    }
}