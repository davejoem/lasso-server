module.exports = (agent)=>{
  return (request, mode, mod) => {
    var responseData = ""
    , op = function(res) {
      res.on('data', (chunk) => {responseData += chunk;})
        .on('error', (err) => {
          agent.utils.log({log: `${mod}-GET: Response stream error!`, err: err})
          reject(err)
        })
        .on('end', () => {
          var responseObject;
          try  {
            responseObject = JSON.parse(responseData)
          } catch (err) {
            agent.utils.log({log: `${mod}-GET: Response wasn't valid JSON.`, err: err})
            resolve(responseData)
          }
          agent.utils.log({log: path + `${mod}-GET: Response success.`, success: "Item found"})
          resolve(responseObject)
        })
    }    
    
    return new Promise((resolve, reject)=>{
      if (request) {
        switch (mode) {
          case 'secure':
            agent.https.get(request, op).on('error', (err) => {
              agent.utils.log({log: `${mod}-GET: Web request error!`, err: err})
              reject(err)
            })
            break
          case 'insecure':
            agent.http.get(request, op).on('error', (err) => {
              agent.utils.log({log: `${mod}-GET: Web request error!`, err: err})
              reject(err)
            })
            break
          default:
            agent.http.get(request, op).on('error', (err) => {
              agent.utils.log({log: `${mod}-GET: Web request error!`, err: err})
              reject(err)
            })
        }
      }
      else {
        agent.utils.log({log: `${mod}-GET: Request path error!`, err: new Error("Expected at least a request")})
        reject(err)
      }      
    })
  }
}