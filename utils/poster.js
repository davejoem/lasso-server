'use strict'

module.exports = (agent) => {
  return class {
    constructor() {
      
    }
    obtain(req) {
  		return new Promise((resolve,reject) => {
        if (req) {
          if (typeof req !== "object") reject(new TypeError("The first argument must be an object."))
          agent.http.get(req, (res) => {
          	res.on('data', (chunk) => {req.poster.write(chunk);})
        		.on('error', (err) => {
        		  if (err) return reject(err)
              agent.fs.unlink(agent.path.resolve(agent.config.postersFolder,req.file), (err) => {
            		if (err) {
            		  agent.utils.log({ log: "POSTER-GET: Response stream error!", err: err})
            		  reject(err);
            	  }
            	})
        		})
          	.on('end', () => {
           		req.poster.end();
           		agent.utils.log({ log: "POSTER-GET: " + req.item + "'s poster was successfully saved.", success: "Saved!"});
        	   	resolve(req.file); 
      	    })
          	}).on('error', (err) => {
    	        agent.utils.agent.utils.log({ log: "POSTER-GET: Web request error!", err: err});
	            reject(err); 
            });
        	}
        	else {
          	let err = new Error("Expects first argument to be an object or a string.");
          	agent.utils.agent.utils.log({ log: "POSTER-GET: Request object/path error!", err: err});
          	reject(err); 
        	}
  		});        
    }
   search(query) {
	  return new Promise((resolve,reject)=>{
      if (query) {
        if ((!query.poster || query.poster == "N/A") && query.poster_path) {
          query.poster = query.poster_path
        }
        var parts = query.poster.split('.')
				var ext = parts[parts.length-1]
        if (!query.imdbid) query.imdbid = query.imdb_id
        var options = {
          file: query.imdbid +'.'+ ext
          , host: agent.url.parse(query.poster).host
          , item: query.title
          , mode: agent.https
          , path: agent.url.parse(query.poster).pathname
          , port: 80
				}          				
        agent.fs.readdir(agent.config.postersFolder, (err, files) => {
  				console.log("looking for ",options.file);
  				if (err) {
    				agent.utils.log({ log: "[POSTER] Couldn't read posters folder!", err: err});
            reject(err);
    			}
        	agent.fs.stat(agent.path.resolve(agent.config.postersFolder,options.file), (err, stats)=>{
        	  if (err) {
				      options.poster = agent.fs.createWriteStream(agent.path.resolve(agent.config.postersFolder,options.file));
        			agent.utils.get(options, 'insecure', 'POSTER').then(
    	    			(poster) => resolve(options.file)
                , (err) => reject(err)
      				);
      				return
        		}
      			if (stats.size > 0) {
      				console.log("Skipped "+options.item+"'s poster. Already exists. Size: ",stats.size);
    	    			agent.utils.log({ log: options.item + "'s poster already exists.", info: options.item + "'s poster already exists."});
        				resolve(options.file); //same as return cb(null,options.file);
      			}
      			else {
      				console.log("Redownloading "+options.item+"'s poster. Size too small. Size: ",stats.size);
    	    			options.poster = agent.fs.createWriteStream(agent.path.resolve(agent.config.postersFolder,options.file));
      				this.obtain(options).then(
  	    				(poster) => resolve(options.file)
                , (err) => reject(err)
    					);
      			}
        	})
    	  })
      }
    })
   }
  }
}