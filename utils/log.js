'use strict';

module.exports = (agent) => {
  return (log) => {
  	let type
  	if ('info' in log) type = 'info'
  	if ('error' in log) type = 'error'
  	if ('warn' in log) type = 'warning'
  	if (Array.isArray(log.log)) {
  		log.log.forEach(l => {
  			process.stdout.write(`${type}: ${l}\n`)
  		})
  	} else {
  		process.stdout.write(`${type}: ${ log.log }\n`)
  	}
  	
    return new Promise((resolve,reject)=>{
      if (log.success) {
        agent.fs.appendFile(agent.config.logFile, "\n["+Date.now()+"] [SUCCESS] "+log.log, (err) => {
          if (err) return reject(err);
          resolve('Logged!');
        });
      }
      if (log.info) {
        agent.fs.ensureFile(agent.path.resolve(__dirname, `..`, `fs`,'logs','info.log'), (err) => {
          if (err) return reject(err);
          agent.fs.appendFile(agent.path.resolve(__dirname, `..`, `fs`,'logs','info.log'), "\n["+Date.now()+"] [INFO] "+log.log, (err) => {
            if (err) return reject(err);
            resolve('Logged!');
          });
        })
      }
      if (log.warn) {
        agent.fs.ensureFile(agent.path.resolve(__dirname, `..`, `fs`,'logs','warnings.log'), (err) => {
          if (err) return reject(err);
          agent.fs.appendFile(agent.path.resolve(__dirname, `..`, `fs`,'logs','warnings.log'), "\n["+Date.now()+"] [WARNING] "+log.log, (err) => {
            if (err) return reject(err);
            resolve('Logged!');
          });
        })
      }
      if (log.error) {
        agent.fs.ensureFile(agent.path.resolve(__dirname, `..`, `fs`,'logs','errors.log'), (err) => {
          if (err) return reject(err);
          agent.fs.appendFile(agent.path.resolve(__dirname, `..`, `fs`,'logs','errors.log'), "\n["+Date.now()+"] [ERROR] "+log.log, (err) => {
            if (err) return reject(err);
            resolve('Logged!');
          });
        })
      }
    })
  }    
}