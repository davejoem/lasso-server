module.exports = (agent) => {
  agent.app.get('/', (req, res, next) => {
  	res.sendFile(agent.path.resolve(__dirname, `..`, `client`, `www`, `index.html`))
  })
  
  agent.app.get('/ip', (req, res) => {
    res.status(200).send(JSON.stringify({ ip: req.headers['x-forwarded-for'] }))
  })
  
  agent.app.get('/socket', (req, res) => {
  	let addr = agent.server.address()
  	, host = req.headers['host']
  	, port = addr.port
  	, protocol = req.secure ? `https://` : `http://`
  	, saddr = addr.address
  	
    res.status(200).send({
    	host: host
    	, protocol: protocol
    	, port: port
    	, saddr: saddr
    	, address: `${protocol}${host}:${addr.port}`
    })
  })
  
  agent.app.get('/*', (req, res, next) => {
    res.redirect('//' + req.headers.host)
  })
}