module.exports = (agent) => {
  agent.app.post('/admin/login', (req, res, next) => {
    if (!req.body.ip || req.body.ip == '')
      return res.status(400).json({ message: "Please enter a valid IP address." })
    if (!req.body.username || req.body.username == '')
      return res.status(400).json({ message: "Please enter a username." })
    if (!req.body.password)
      return res.status(400).json({ message: "Please enter a password." })
    process.stdout.write(`${JSON.stringify(req.body)}`)
    let makeToken = () => {
      let today = new Date()
      let exp = new Date(today)
      // set expiration to 1 day
      exp.setDate(today.getDate() + 1)
      return agent.jwt.sign({
        username: req.body.username,
        exp: parseInt(exp.getTime() / 1000),
      }, agent.config.appname)
    }
    // process.stdout.write(`\nConnecting to router at ${req.body.ip} using username ${req.body.username} and password ${req.body.password}\n`)
    if (!agent.router.connected) {
      console.log('router is connected')
      agent.router.connect(
        req.body.ip
        , req.body.username
        , req.body.password
      ).then(
        conn => {
          process.stdout.write(`\nConnected to router :-)\n`)
          agent.config.router.ip = agent.router.ip = req.body.ip
          agent.config.router.username = agent.router.username = req.body.username
          agent.config.router.password = agent.router.password = req.body.password

          let config_path = agent.path.resolve(__dirname, '..', 'config.json')
          process.stdout.write(`Path: ${config_path}`)
          agent.fs.unlink(config_path, err => {
            if (err)
              return res.status(500).send({ status: 'error', message: `${JSON.stringify(err)}` })
            agent.fs.ensureFile(config_path).then(
              () => {
                var stream = agent.fs.createWriteStream(config_path);
                stream.once('open', function (fd) {
                  stream.write("My first row\n");
                  stream.write("My second row\n");
                  stream.end();
                  res.status(200).json({ token: makeToken() })
                });
                //       let opts = {
                //         encoding: 'utf8'
                //         , mode: 0o666
                //         , flag: 'w'
                //       }, cnfg = agent.config
                //       process.stdout.write(`Config : ${JSON.stringify(cnfg)}`)
                //       agent.fs.writeFile(agent.path.resolve(__dirname, '..', 'config.json'), JSON.stringify(cnfg), opts, err=>{
                //         if (err)
                //           return res.status(500).send({ status: 'error', message: `${JSON.stringify(err)}` })
                //         res.status(200).send({ status: 'success', token: agent.router.username })
                //       })
                //     }
                //     , err=>res.status(500).send({ status: 'error', message: `${JSON.stringify(err)}` })
              }
            )
          })
        }
        , err => {
          if (err.errors[0].message == `cannot log in`)
            res.status(401).json({ status: 'failed', message: `Incorrect username or password.` })
          else
            res.status(500).json({ status: 'error', message: `An error occured. Please try again.` })
        }
        )
    }
    else {
      console.log('router is connected')
      res.status(200).json({ token: makeToken() })
    }
  })
  agent.app.post('/admin/users', (req, res, next) => {
    if (!req.body.token)
      return res.status(401).send()
    if (!agent.router.connected)
      return res.status(503).send()
    agent.router.getUsers().then(
      users => {
        this.users = users
        res.status(200).json({ users: users })
      }
      , () => res.status(200).json({ users: agent.router.users })
    )
  })
  agent.app.get('/admin/service/available', (req, res, next) => {
    res.status(200).json({ available: agent.router.connected })
  })
  agent.app.post('/admin/logout', (req, res, next) => {
    if (!req.body.token)
      return res.status(401).json({})
    agent.jwt.verify(req.body.token, agent.config.appname, (err, payload) => {
      if (err)
        return res.status(401).json({ error: new Error(err.message) })
      if (agent.router.connected) {
        agent.router.disconnect()
        res.status(200).json({
          title: `Logged out!`
          , message: `Lasso will no longer be able to manage clients that connect to your Hotspot`
        })
      }
      else
        res.status(200).json({
          title: `Logged out!`
          , message: `Lasso will no longer be able to manage clients that connect to your Hotspot`
        })
    })
  })
  agent.app.post('/admin/loggedin', (req, res, next) => {
    if (!req.body.token)
      return res.status(401).json({ status: `failed`, message: `Unauthorized.` })
    else {
      if (req.body.token == 'pre')
        res.status(200).json({ loggedin: agent.router.connected })
      else {
        agent.jwt.verify(req.body.token, agent.config.appname, (err, payload) => {
          if (err)
            return res.status(401).json({ status: `Unverified`, error: new Error(err.message), message: `Unauthorized.` })
          res.status(200).send({ loggedin: agent.router.connected })
        })
      }
    }
  })
  agent.app.get('/admin', (req, res, next) => {
    res.sendFile(agent.path.resolve(__dirname, `..`, `admin`, `www`, `index.html`))
  })
}