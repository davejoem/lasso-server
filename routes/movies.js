module.exports = (agent)=>{
  agent.app.get('/movies/list/all', (req,res,next)=>{
    agent.models.Movie.find().exec().then(
      movies=>{
        res.status(200).json({ status: 'success', movies: movies })
      }
      , ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again.` })
    )
  })
  agent.app.post('/movies/list/all', (req,res,next)=>{
    agent.models.Movie.find().exec().then(
      movies=>{
        res.status(200).json({ status: 'success', movies: movies })
      }
      , ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again.` })
    )
  })
}