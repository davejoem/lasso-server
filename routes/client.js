module.exports = (agent) => {
  agent.app.get('/client', (req, res, next) => {
    res.sendFile(agent.path.resolve(__dirname, `..`, `client`, `www`, `index.html`))
  })
}