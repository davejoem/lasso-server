module.exports = (agent)=>{
  agent.app.get('/business/payments/all', (req,res)=>{
    agent.models.Transaction.find().exec().then(
      transactions=>res.status(200).json({ transactions: transactions })
      , err=>res.status(500).json({ error: err })
    )
  })
  agent.app.get('/business/payments/confirmation', (req,res)=>{
    if (req.query.phone != 'MPESA') return res.status(400).end()
    process.stdout.write(`\n\n`)
    process.stdout.write(`PAYMENT CONFIRMATION BY GET${JSON.stringify(req.query)}`)
    process.stdout.write(`\n\n`)
    let message = req.query.text
    let parts = message.split(' ')
    let transactioncode = parts[0]

    let dateparts = parts[2].split('/').reverse()
    let year = `20${dateparts[0]}`
    let month = dateparts[1].length > 1
                  ? dateparts[1]
                  : `0${dateparts[1]}`
    let day = dateparts[2].length > 1
                ? dateparts[2]
                : `0${dateparts[2]}`
    let timeparts = parts[4]
    let suf = parts[5].charAt(0)+parts[5].charAt(1)
    let hour = timeparts.split(':')[0].length > 1
                ? suf == 'PM'
                    ? (parseInt(timeparts.split(':')[0])+12).toString()
                    : timeparts.split(':')[0]
                : suf == 'PM'
                    ? `0${(parseInt(timeparts.split(':')[0])+12)}`
                    : `0${timeparts.split(':')[0]}`
    let min = timeparts.split(':')[1]

    let time = hour + ":" + min
    let date = year+month+day+" "+time


    let transaction = agent.models.Transaction()
    transaction.amount = parseFloat(parts[5].split('Ksh')[1])
    transaction.carrier = req.query.phone
    transaction.code = transactioncode
    transaction.date = new Date(agent.moment().toISOString())
    transaction.sender.name = parts[9] + " " + parts[10].split('.')[0]
    transaction.sender.number = parseInt(parts[8].split('254')[1])
    console.log(transaction)
    transaction.save().then(
      transaction=>{
        let url =  `${agent.config.sms.api}/send?device=${agent.config.sms.device}&email=${agent.config.sms.email}&password=${agent.config.sms.password}&number=+254${transaction.sender.number}&message=Payment received. Login at ${agent.config.server.address} and re-enter the following code to get access to paid content. You may also share it.`
        , url2 =  `${agent.config.sms.api}/send?device=${agent.config.sms.device}&email=${agent.config.sms.email}&password=${agent.config.sms.password}&number=+254${transaction.sender.number}&message=${transaction.code}`
        function send(target) {
          return new Promise((resolve, reject)=>{
            agent.request.get({
              headers: {
                'Accepts': 'application/json'
              },
              url: target
            }).on('response', resolve)
            .on('error', reject)
          })
        }
        Promise.all([
          send(url)
          , send(url2)
        ]).then(()=>res.status(200).json({ status: 'success' }))
          .catch(()=>res.status(200).json({ status: 'warning', message: "Confirmed but couldn't send alert to client." }))
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
    )
  })
  agent.app.post('/business/payments/confirmation', (req,res)=>{
    process.stdout.write(`\n\n`)
    process.stdout.write(`PAYMENT CONFIRMATION BY POST${JSON.stringify(req.body)}`)
    process.stdout.write(`\n\n`)
  })
  agent.app.post('/business/payments/recieved', (req,res,next)=>{
    process.stdout.write(`PAYMENT RECEPTION BY POST${JSON.stringify(req.body)}`)
    if (!req.body.phonenumber) return res.status(400).json({ status: `failed`, message: `Please enter a phonenumber.` })
    if (!req.body.transactioncode) return res.status(400).json({ status: 'failed', message: `Please enter the transaction code.` })
    agent.models.Transaction.findOne({ "code": req.body.transactioncode.toUpperCase() }).exec().then(
      transaction=>{
        if (transaction != null){
          if (transaction.used)
            return res.status(200).json({ status: `used` })
          else 
            agent.models.User.findOne({ "accounts.local.phonenumber": req.body.phonenumber }).exec().then(
              user=>{
                if (user != null) {
                  transaction.user = user._id
                  transaction.used = true
                  transaction.save().then(
                    t=>{
                      user.finances.transactions.push(transaction._id)
                      user.finances.balance += transaction.amount
                      user.save().then(
                        us=>res.status(200).json({ status: "success", user: us})
                        , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
                      )
                    }
                    , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
                  )
                }
                else
                  res.status(400).json({ status: 'error', message: `User not found` })
              }
              , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
            )
        }
        else
          res.status(400).json({ status: 'error', message: `Transaction not found` })
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
    )
  })
  agent.app.post('/business/bundles/buy', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}\n`)
    if (!req.body.phonenumber) return res.status(400).json({ status: `failed`, message: `Please enter a phonenumber` })
    if (!req.body.bundle) return res.status(400).json({ status: `failed`, message: `Please choose a bundle` })
    if (!req.body.session) return res.status(400).json({ status: `failed`, message: `Please enter session details` })
    
    function checkUserExists(username) {
      return new Promise((resolve, reject)=>{
        let commands = [
          `/ip/hotspot/user/print`
          , `?name=${username}`
        ]
        process.stdout.write(`Running ${JSON.stringify(commands)}`)
        agent.router.conn.getCommandPromise(commands).then(user=>resolve(user.length > 0), reject(false))
      })
    }
    
    function createNewSpeedUser(user) {
      process.stdout.write(`creating new speed user\n`)
      agent.router.addUser({
        mac: req.body.session.mac
        , password: `${user.accounts.local.phonenumber}password`
        , type: 'speed'
        , profile: `${req.body.bundle.profile}`
        , bytes: req.body.bundle.bytes
      }).then(
        r=>user.startCredit(req.body.bundle.cost, req.body.session.mac).then(
          ()=>res.status(200).json({ username: `${req.body.session.mac}`, password: `${user.accounts.local.phonenumber}password` })  
          , ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
        )
    		, ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
    	)
    }
    
    function createNewSizeUser(user) {
      process.stdout.write(`Creating new size user\n`)
      agent.router.addUser({
        mac: req.body.session.mac
        , password: `${user.accounts.local.phonenumber}password`
        , type: 'size'
        , profile: `=4Mbps_Profile`
        , bytes: req.body.bundle.bytes
      }).then(
        r=>user.creditOnce(req.body.bundle.cost, 'balance').then(
          u=>u.save().then(
            us=>res.status(200).json({ username: `${req.body.session.mac}`, password: `${us.accounts.local.phonenumber}password` })
            , ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
          )
        )
    		, ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
      )
    }
    
    function updateSizeUser(user) {
      agent.router.updateUser({
        mac: req.body.session.mac
        , bytes: req.body.bundle.bytes
      }).then(
        r=>res.status(200).json({ username: `${req.body.session.mac}`, password: `${user.accounts.local.phonenumber}password` })
    		, ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
    	)  
    }
    
    function updateSpeedUser(user) {
      process.stdout.write(`Updating speed user\n`)
      agent.router.updateUser({
        mac: req.body.session.mac
        , profile: req.body.bundle.profile
      }).then(
        re=>user.startCredit(req.body.session.mac).then(
          ()=>res.status(200).json({ username: `${req.body.session.mac}`, password: `${user.accounts.local.phonenumber}password` })
          , ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
        )
        , ()=>res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
      )
    }
    
    agent.models.User.findOne({ 'accounts.local.phonenumber': req.body.phonenumber }).exec().then(
      user=>{
        if (user != null) {
          checkUserExists(req.body.session.mac).then(
            a=>{
              req.body.bundle.type == 'speed'
                ? a == false
                  ? createNewSpeedUser(user)
                  : updateSpeedUser(user)
                : req.body.bundle.type == 'size'
                  ? a == false
                    ? createNewSizeUser(user)
                    : updateSizeUser(user)
          	      : null
            }
            , err=> {
        		  process.stdout.write(`Router check user error: ${JSON.stringify(err)}`);
        		  res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
        		}
          )
        }
        else {
          process.stdout.write(`User not found.`);
          res.status(400).json({ status: 'error', message: 'User not found.'})
        }
      }
      , err=> {
  		  process.stdout.write(`Database user error: ${JSON.stringify(err)}\n`);
  		  res.status(500).json({ status: 'error', message: `An error occured. Please try again` })
  		}
    )
  })
}