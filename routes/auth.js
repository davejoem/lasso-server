module.exports = (agent)=>{
  agent.app.post('/auth/user/check', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}\n`)
    if (!req.body.phonenumber) return res.status(400).json({ status: `failed`, message: "Please enter a phonenumber" })
    agent.models.User.findOne({ "accounts.local.phonenumber": req.body.phonenumber }).exec().then(
      user=>{
        if (user && user != null) res.status(200).json({ status: 'exists', user: user })
        else res.status(200).json({ status: '!exists' })
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
    )
  })
  agent.app.post('/auth/user/refresh', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}\n`)
    if (!req.body.phonenumber) return res.status(400).json({ status: `failed`, message: "Please enter a phonenumber" })
    agent.models.User.findOne({ "accounts.local.phonenumber": req.body.phonenumber }).exec().then(
      user=>{
        if (user && user != null) 
          res.status(200).json({ status: "success", user: user})
        else
          res.status(400).json({ status: '!exists', message: `User not found` })
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
    )
  })
  agent.app.post('/auth/user/newcode', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}\n`)
    if (!req.body.phonenumber) return res.status(400).json({ status: "failed", message: "Please enter a phonenumber" })
    agent.models.User.findOne({ "accounts.local.phonenumber": req.body.phonenumber }).exec().then(
      user=>{
        if (user && user != null)
          user.sendActivationCode('phone').then(
            user=>
              user.save().then(
                usr=>{
                  process.stdout.write(`Activation code: ${usr.accounts.local.activation.activationcode}\n`)
                  res.status(200).json({ status: "success", user: usr })
                }
                , err=>res.status(500).json({ status: 'error', message: `An error occured. Please try again`, error: err })
              )
            , err=>res.status(500).json({ status: 'error', message: `An error occured. Please try again`, error: err })
          )
        else
          res.status(400).json({ status: '!exists', message: `User not found` })
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured. Please try again`, error: err })
    )
  })
  agent.app.post('/auth/user/signup', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}`)
    if (!req.body.phonenumber)
      return res.status(400).json({
        status: "failed",
        error: "Please enter a phonenumber"
      })
    let newUser = new agent.models.User()
    let num
    if (req.body.phonenumber.startsWith('254')  || req.body.phonenumber.startsWith('+254')) {
      if (req.body.phonenumber.startsWith('254'))
        req.body.phonenumber = `+` + req.body.phonenumber
      num = parseInt(req.body.phonenumber.split('+254')[1])
    }
    if (req.body.phonenumber.startsWith('0'))
      num = parseInt(req.body.phonenumber)
    newUser.accounts.local.phonenumber = num
    console.log(newUser)
    newUser.sendActivationCode('phone').then(
      user=>
        user.save().then(
          usr=>
            usr.sendActivationCode('none').then(
              us=>res.status(200).json({ status: "success" })
              , err=>res.status(500).json({ status: 'error', message: `An error occured. Please try again`, error: err })
            )
          , err=>res.status(500).json({ status: 'error', message: `An error occured. Please try again`, error: err })
        )
      , err=>{
        console.log("Error sending code", err)
        res.status(500).json({ status: 'error', message: `An error occured. Please try again`, error: err })
      }
    )
  })
  agent.app.post('/auth/user/activate', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}\n`)
    if (!req.body.phonenumber) return res.status(400).json({ status: `failed`, message: "Please enter a phonenumber" })
    if (!req.body.activationcode) return res.status(400).json({ status: `failed`, message: "Please enter a activation code" })
    agent.models.User.findOne({ "accounts.local.phonenumber": req.body.phonenumber }).exec().then(
      user=>{
        if (user && user != null) {
          user.activate(req.body.activationcode).then(
            user=>
              user.save().then(
                user=>res.status(200).json({ status: "success", user: user })
                , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
              )
            , err=>res.status(400).json({ status: 'error', message: err.message })
          )
        }
        else
          res.status(400).json({ status: '!exists', message: `User not found` })
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
    )
  })
  agent.app.post('/auth/user/signin', (req,res,next)=>{
    process.stdout.write(`${JSON.stringify(req.body)}\n`)
    if (!req.body.phonenumber) return res.status(400).json({ status: `failed`, message: "Please enter a phonenumber" })
    if (!req.body.activationcode) return res.status(400).json({ status: `failed`, message: "Please enter a activation code" })
    agent.models.User.findOne({ "accounts.local.phonenumber": req.body.phonenumber }).exec().then(
      user=>{
        if (user && user != null) 
          user.activate(req.body.activationcode).then(
            usr=>
              usr.save().then(us=>{
                res.status(200).json({ status: "success", user: us})
              })
            , err=>
              res.status(400).json({ status: 'error', message: err.message })
          )
        else
          res.status(400).json({ status: '!exists', message: `User not found` })
      }
      , err=>res.status(500).json({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
    )
  })
}