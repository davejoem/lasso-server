'use strict'

module.exports = (agent) => {
  function sendError(err, message, cb) {
    cb({
      err: err,
      message: {
        class: "error",
        message: message
      }
    })
  }
  function sendWarning(warn, message, cb) {
    cb({
      warn: warn,
      message: {
        class: "warning",
        message: message
      }
    })
  }
  function sendInfo(info, message, cb) {
    cb({
      info: info,
      message: {
        class: "info",
        message: message
      }
    })
  }
  agent.io.on('connection', socket => {
    socket.on('device:register', (data, cb) => {
      var device = new agent.models.Device(data);
      device.generateToken((e, token) => {
        if (e)
          return sendError(e, "Couldn't register this device.", cb)
        device.save((e, device) => {
          if (e)
            return sendError(e, "Couldn't register this device.", cb)
          cb(null, {
            token: token,
            device: device
          }, {
              message: {
                class: "success",
                message: "Device registered successfully."
              }
            })
        })
      })
    })
    socket.on('device:id', (id, cb) => {
      agent.jwt.verify(id, agent.config.device, (e, payload) => {
        if (e) {
          socket.emit('device:id:error')
          return sendError(e, "Device verification error.", cb)
        }
        agent.models.Device.findById(payload.id, (e, device) => {
          if (e) {
            socket.emit('device:id:error')
            return sendError(e, "Device verification error.", cb)
          }
          if (device) {
            socket['device'] = device
            cb(null, device)
          }
          else {
            sendInfo(new Error(`Device doesn't exist`), `Device doesn't exist`, cb)
          }
        })
      })
    })
    socket.on('device:deregister', (id, cb) => {
      agent.jwt.verify(id, agent.config.device, (e, payload) => {
        if (e) {
          socket.emit('device:deregister:error')
          return sendError(e, "Device verification error.", cb)
        }
       	 agent.models.Device.findById(payload.id, (e, device) => {
          if (e) {
            socket.emit('device:deregister:error')
            return sendError(e, "Device verification error.", cb)
          }
          if (device) {
            console.log(`events/device.js, 85, 12, write code to deregister device here`)
            cb(null, device)
          }
          else {
          	 let msg = `Device doesn't exist`
            sendInfo(new Error(msg), msg, cb)
          }
        })
      })
    })
  })
}