'use strict'

module.exports = (agent) => {
  function sendError(err, message, cb) {
    cb({
      err: err,
      message: {
        class: "error",
        message: message
      }
    })
  }
  function sendWarning(warn, message, cb) {
    cb({
      warn: warn,
      message: {
        class: "warning",
        message: message
      }
    })
  }
  function sendInfo(info, message, cb) {
    cb({
      info: info,
      message: {
        class: "info",
        message: message
      }
    })
  }
  agent.io.on('connection', socket => {
    socket.on('movie:list:all', (data, cb) => {
    	agent.models.Movie.find().exec().then(
      movies => {
        cb(null, { status: 'success', movies: movies })
      }
      , () => {
      	let msg = `An error occured. Please try again.`
        sendError(new Error(msg), msg, cb)
    	}
    )
    })
	})
}