module.exports = (agent)=>{
  function sendError(err, message, cb) {
    cb({
      err: err
      , message: {
        class: "error",
        message: message.message || message
      }
      , status: message.status
    })
  }
  agent.io.on('connection', socket=>{
    process.stdout.write(`User connected\n`)
    agent.app.post('admin:login', (data, cb)=>{
      if (!data.ip || data.ip == '')
        return sendError(null, { message: "Please enter a valid IP address." }, cb)
      if (!data.username || data.username == '')
        return sendError(null, { message: "Please enter a username." }, cb)
      if (!data.password)
        return sendError(null, { message: "Please enter a password." }, cb)
      process.stdout.write(`${JSON.stringify(data)}`)
      let makeToken = ()=>{
        let today = new Date()
        let exp = new Date(today)
        // set expiration to 1 day
        exp.setDate(today.getDate() + 1)
        return agent.jwt.sign({
          username: data.username,
          exp: parseInt(exp.getTime() / 1000),
        }, agent.config.appname)
      }
      // process.stdout.write(`\nConnecting to router at ${data.ip} using username ${data.username} and password ${data.password}\n`)
      if (!agent.router.connected) {
        console.log('router is connected')
        agent.router.connect(
          data.ip
          , data.username
          , data.password
        ).then(
          conn=>{
            process.stdout.write(`\nConnected to router :-)\n`)
            agent.config.router.ip = agent.router.ip = data.ip
            agent.config.router.username = agent.router.username = data.username
            agent.config.router.password = agent.router.password = data.password
  
            let config_path = agent.path.resolve(__dirname, '..', 'config.json')
            process.stdout.write(`Path: ${config_path}`)
            agent.fs.unlink(config_path, err=>{
              if (err)
                return sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
              agent.fs.ensureFile(config_path).then(
                ()=>{
                  var stream = agent.fs.createWriteStream(config_path);
                  stream.once('open', function(fd) {
                    stream.write("My first row\n");
                    stream.write("My second row\n");
                    stream.end();
                    cb(null, { token: makeToken() })
                  });
              //       let opts = {
              //         encoding: 'utf8'
              //         , mode: 0o666
              //         , flag: 'w'
              //       }, cnfg = agent.config
              //       process.stdout.write(`Config : ${JSON.stringify(cnfg)}`)
              //       agent.fs.writeFile(agent.path.resolve(__dirname, '..', 'config.json'), JSON.stringify(cnfg), opts, err=>{
              //         if (err)
              //           return sendError(err, { status: 'error', message: `${JSON.stringify(err)}` })
              //         res.status(200).send({ status: 'success', token: agent.router.username })
              //       })
              //     }
              //     , err=>sendError(err, { status: 'error', message: `${JSON.stringify(err)}` })
                }
              )
            })
          }
          , err=>sendError(err, { status: 'error', message: `${JSON.stringify(err)}` })
        )
      }
      else {
        console.log('router is connected')
        cb(null, {  token: makeToken() })
      }
    })
    agent.app.post('admin:users', (data, cb)=>{
      if (!data.token)
        return sendError(null, {status: `failed`, message: `Not authorized.` }, cb)
      if (!agent.router.connected)
        return sendError(null, {status: `failed`, message: `Service unavailable.` }, cb)
      cb(null, { users: agent.router.users })
    })
    agent.app.get('admin:service:available', (data, cb)=>{
      cb(null, { available: agent.router.connected })
    })
    agent.app.post('admin:logout', (data, cb)=>{
      if (!data.token)
        return sendError(null, {status: `failed`, message: `Not authorized.` }, cb)
      agent.jwt.verify(data.token, agent.config.appname, (err,payload)=>{
        if (err)
          return sendError(err, {status: `failed`, message: `An error occured. Please try again.` }, cb)
        if (agent.router.connected) {
          agent.router.disconnect()
          cb(null, {
            title: `Logged out!`
            , message: `Lasso will no longer be able to manage clients that connect to your Hotspot`
          })
        }
        else
          cb(null, {
            title: `Logged out!`
            , message: `Lasso will no longer be able to manage clients that connect to your Hotspot`
          })
      })
    })
    agent.app.post('admin:loggedin', (data, cb)=>{
      if (!data.token)
        return sendError(null, {status: `failed`, message: `Not authorized.` }, cb)
      else {
        if (data.token == 'pre')
          cb(null, { loggedin: agent.router.connected })
        else {
          agent.jwt.verify(data.token, agent.config.appname, (err, payload)=>{
            if (err)
              return sendError(err, {status: `failed`, message: `An error occured. Please try again.` }, cb)
            console.log(payload);
            cb(null, { loggedin: agent.router.connected })
          })
        }
      }
    })
  })
}
