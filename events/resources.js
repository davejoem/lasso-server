module.exports = (agent)=>{
  function sendError(err, message, cb) {
    cb({
      err: err,
      message: {
        class: "error",
        message: message
      }
    })
  }
  function sendWarning(warn, message, cb) {
    cb({
      warn: warn,
      message: {
        class: "warning",
        message: message
      }
    })
  }
  function sendInfo(info, message, cb) {
    cb({
      info: info,
      message: {
        class: "info",
        message: message
      }
    })
  }
  agent.io.on('connection', socket=>{
    agent.ss(socket).on('resource:android:apk', function(stream) {
      stream.pipe(
        agent.fs.createWriteStream(
          agent.path.resolve(__dirname, `..`, `resources`, `android`,`lasso.apk`)
        )
      )
    }).on('resource:windows:desktopclient', function(stream) {
      stream.pipe(
        agent.fs.createWriteStream(
          agent.path.resolve(__dirname, `..`, `resources`, `windows`,`lasso.msi`)
        )
      )
    }).on('resource:mac:desktopclient', function(stream) {
      stream.pipe(
        agent.fs.createWriteStream(
          agent.path.resolve(__dirname, `..`, `resources`, `mac`,`lasso.pkg`)
        )
      )
    }).on('resource:linux:desktopclient', function(stream) {
      stream.pipe(
        agent.fs.createWriteStream(
          agent.path.resolve(__dirname, `..`, `resources`, `linux`,`lasso.deb`)
        )
      )
    })
  })
}
