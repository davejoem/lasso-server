module.exports = (agent)=>{
  function sendError(err, message, cb) {
    cb({
      err: err
      , message: {
        class: "error",
        message: message.message || message
      }
      , status: message.status
    })
  }
  agent.io.on('connection', socket=>{
    socket.on('business:payments:all', (req,res)=>{
    agent.models.Transaction.find((err, transactions)=>{
      if (err)
        return res.status(500).send()
      res.status(200).send({ transactions: transactions })
    })
    })
    socket.on('business:payments:confirmation', (req,res)=>{
      process.stdout.write(`\n\n`)
      process.stdout.write(`PAYMENT CONFIRMATION BY GET${JSON.stringify(req.query)}`)
      process.stdout.write(`\n\n`)
      let message = req.query.text
      let parts = message.split(' ')
      let transactioncode = parts[0]
  
      let dateparts = parts[2].split('/').reverse()
      let year = `20${dateparts[0]}`
      let month = dateparts[1].length > 1
                    ? dateparts[1]
                    : `0${dateparts[1]}`
      let day = dateparts[2].length > 1
                  ? dateparts[2]
                  : `0${dateparts[2]}`
      let timeparts = parts[4]
      let suf = parts[5].charAt(0)+parts[5].charAt(1)
      let hour = timeparts.split(':')[0].length > 1
                  ? suf == 'PM'
                      ? (parseInt(timeparts.split(':')[0])+12).toString()
                      : timeparts.split(':')[0]
                  : suf == 'PM'
                      ? `0${(parseInt(timeparts.split(':')[0])+12)}`
                      : `0${timeparts.split(':')[0]}`
      let min = timeparts.split(':')[1]
  
      let time = hour + ":" + min
      let date = year+month+day+" "+time
  
  
      let transaction = agent.models.Transaction()
      transaction.amount = parseFloat(parts[5].split('Ksh')[1])
      transaction.carrier = req.query.phone
      transaction.code = transactioncode
      transaction.date = new Date(agent.moment().toISOString())
      transaction.sender.name = parts[9] + " " + parts[10].split('.')[0]
      transaction.sender.number = parseInt(parts[8].split('254')[1])
      console.log(transaction)
      transaction.save().then(
        transaction=>{
          let url =  `${agent.config.sms.api}/send?device=${agent.config.sms.device}&email=${agent.config.sms.email}&password=${agent.config.sms.password}&number=+254${transaction.sender.number}&message=Transaction code ${transaction.code} was recieved. Login at ${agent.config.server.address} and re-enter this code to get access to paid content. You may also share it`
          console.log("URL: ",url)
          agent.request.get({
            headers: {
              'Accepts': 'application/json'
            },
            url: url
          }).on('response', response=>res.status(200).send("Confirmed. Alert sent to client."))
          .on('error', error=>res.status(200).send("Confirmed but couldn't send alert to client."))
        }
        , err=>res.status(500).send({ status: 'error', message: `An error occured ${JSON.stringify(err)}` })
      )
    })
    socket.on('business:payments:confirmation', (data, cb)=>{
      process.stdout.write(`\n\n`)
      process.stdout.write(`PAYMENT CONFIRMATION BY POST${JSON.stringify(data)}`)
      process.stdout.write(`\n\n`)
    })
    socket.on('business:payments:recieved', (data,cb)=>{
      process.stdout.write(`PAYMENT RECEPTION BY POST${JSON.stringify(data)}`)
      if (!data.phonenumber) return sendError(null, { status: `failed`, message: `Please enter a phonenumber.` }, cb)
      if (!data.transactioncode) return sendError(null, { status: 'failed', message: `Please enter the transaction code.` }, cb)
      agent.models.Transaction.findOne({ "code": data.transactioncode.toUpperCase() }).exec().then(
        transaction=>{
          if (transaction != null){
            if (transaction.used)
              return cb(null, { status: 'used' })
            else 
              agent.models.User.findOne({ "accounts.local.phonenumber": data.phonenumber }).exec().then(
                user=>{
                  if (user != null) {
                    transaction.user = user._id
                    transaction.used = true
                    transaction.save().then(
                      t=>{
                        user.finances.transactions.push(transaction._id)
                        user.finances.balance = transaction.amount
                        user.save().then(
                          us=>cb(null, { status: "success", user: us})
                          , err=>sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
                        )
                      }
                      , err=>sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
                    )
                  }
                  else
                    sendError(null, { status: 'error', message: `User not found` }, cb)
                }
                , err=>sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
              )
          }
          else
            sendError(null, { status: 'error', message: `Transaction not found` }, cb)
        }
        , err=>sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
      )
    })
    socket.on('/business/buybundle', (data,cb)=>{
      process.stdout.write(`${JSON.stringify(data)}\n`)
      if (!data.phonenumber) return sendError(null, { status: `failed`, message: `Please enter a phonenumber` }, cb)
      if (!data.bundle) return sendError(null, { status: `failed`, message: `Please choose a bundle` }, cb)
      agent.models.User.findOne({ 'accounts.local.phonenumber': data.phonenumber }).exec().then(
        user=>{
          if (user != null) {
            switch(data.bundle.type) {
              case 'speed':
                process.stdout.write(`\n\nRouter : ${JSON.stringify(agent.utils.router)}\n\n`)  
                agent.utils.router.conn.getCommandPromise([
                  '/ip/hotspot/user/add'
                  , `=name=${user.accounts.local.phonenumber}number`
                  , `=password=${user.accounts.local.phonenumber}password`
                  , `=profile=${data.bundle.profile}`
                ]).then(
                  r=> {
                    console.log("create user response: ", r)
                    let minus = ()=>{
                      if ((user.finances.balance + user.finances.lp) > data.bundle.cost) {
                        if (user.finances.balance > data.bundle.cost) user.finances.balance -= data.bundle.cost
                        else user.finances.lp -= data.bundle.cost
                        user.save()
                      }
                    }
                    let int = setInterval(minus, 1000)
                    console.log(`User ${user.accounts.local.phonenumber} crediting`)
                    agent.utils.router.connections[`${user.accounts.local.phonenumber}number`] = int
                    cb(null, {
                      username: `${user.accounts.local.phonenumber}number`
                      , password: `${user.accounts.local.phonenumber}password`
                    })
              		}
              		, err=> {
              		  process.stdout.write(`Router create user error: ${JSON.stringify(err)}`);
              		  sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
              		}
            	  )
            	  break
              case  'size':
                process.stdout.write(`\n\nRouter : ${JSON.stringify(agent.utils.router)}\n\n`)
                agent.utils.router.conn.getCommandPromise([
                  '/ip/hotspot/user/add'
                  , `=name=${user.accounts.local.phonenumber}number`
                  , `=password=${user.accounts.local.phonenumber}password`
                  , `=profile=4Mbps_Profile`
                  , `=limit-bytes-total=${data.bundle.bytes}`
                ]).then(
                  r=> {
                    console.log("create user response: ", r)
                    user.creditOnce(data.bundle.cost).then(
                      u=>{
                        console.log(`User credited: `, u)
                        u.save().then(
                          us=>{
                            console.log(agent.config.router.ip, agent.config.router.username, agent.config.router.password)
                            cb(null, {
                              username: `${us.accounts.local.phonenumber}number`
                              , password: `${us.accounts.local.phonenumber}password`
                            })   
                          }
                          , err=>sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
                        )
                      }
                    )
              		}
              		, err=> {
              		  process.stdout.write(`Router create user error: ${JSON.stringify(err)}`);
              		  sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
              		}
            	  )
            	  break
            }
          }
          else
            sendError(null, { status: 'error', message: 'User not found.'}, cb)
        }
        , err=>sendError(err, { status: 'error', message: `An error occured. Please try again.` }, cb)
      )
    })
  })
}
