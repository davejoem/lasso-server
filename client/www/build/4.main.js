webpackJsonp([4],{

/***/ 758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RedeemLPModule", function() { return RedeemLPModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__redeem_lp__ = __webpack_require__(769);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RedeemLPModule = (function () {
    function RedeemLPModule() {
    }
    return RedeemLPModule;
}());
RedeemLPModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__redeem_lp__["a" /* RedeemLP */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__redeem_lp__["a" /* RedeemLP */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__redeem_lp__["a" /* RedeemLP */]
        ]
    })
], RedeemLPModule);

//# sourceMappingURL=redeem-lp.module.js.map

/***/ }),

/***/ 769:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RedeemLP; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(421);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RedeemLP = (function () {
    function RedeemLP(user, viewCtrl) {
        this.user = user;
        this.viewCtrl = viewCtrl;
    }
    RedeemLP.prototype.cancel = function () {
        this.viewCtrl.dismiss('cancel');
    };
    RedeemLP.prototype.ngOnInit = function () {
        this.refresh();
    };
    RedeemLP.prototype.redeem = function () {
        this.viewCtrl.dismiss('redeem');
    };
    RedeemLP.prototype.refresh = function () {
        var _this = this;
        this.user.refresh(this.user._user.accounts.local.phonenumber).subscribe(function (res) {
            _this.user.save(res.user).then(function () {
                _this.lp = res.user.finances.lp;
            });
        });
    };
    return RedeemLP;
}());
RedeemLP = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'redeem-lp',template:/*ion-inline-start:"/home/ubuntu/workspace/client/src/pages/redeem-lp/redeem-lp.html"*/'<ion-header>\n\n  <ion-toolbar no-border-bottom>\n\n    <h5 class="lasso" text-center>Redeem Lasso Points</h5>\n\n    <ion-buttons end>\n\n      <button ion-button clear small (click)="refresh">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <p>You currently have <i><b>Ksh. {{balance}}</b></i> and <i><b>{{lp}} LP</b></i> on your account.</p>\n\n    <br>\n\n    <p *ngIf="lp < 10">You have insufficient LP. There ain\'t any offers that you can redeem {{lp}} LP for.</p>\n\n    <ion-row wrap *ngIf="lp >= 10">\n\n      <ion-card></ion-card>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-col></ion-col>\n\n  <ion-col>\n\n    <button ion-button clear color="danger" (click)="cancel()">{{ lp < 10 ? \'Ok\' : \'Cancel\' }}</button>\n\n  </ion-col>\n\n  <ion-col *ngIf="lp >= 10"></ion-col>\n\n  <ion-col *ngIf="lp >= 10">\n\n    <button ion-button full color="secondary" (click)="redeemLP()">Redeem</button>\n\n  </ion-col>\n\n  <ion-col></ion-col>\n\n</ion-footer>\n\n'/*ion-inline-end:"/home/ubuntu/workspace/client/src/pages/redeem-lp/redeem-lp.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_providers__["b" /* User */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
], RedeemLP);

//# sourceMappingURL=redeem-lp.js.map

/***/ })

});
//# sourceMappingURL=4.main.js.map