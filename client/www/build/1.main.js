webpackJsonp([1],{

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionCodeModule", function() { return TransactionCodeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__transactioncode__ = __webpack_require__(772);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TransactionCodeModule = (function () {
    function TransactionCodeModule() {
    }
    return TransactionCodeModule;
}());
TransactionCodeModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__transactioncode__["a" /* TransactionCode */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__transactioncode__["a" /* TransactionCode */]),
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* ReactiveFormsModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__transactioncode__["a" /* TransactionCode */]
        ]
    })
], TransactionCodeModule);

//# sourceMappingURL=transactioncode.module.js.map

/***/ }),

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionCode; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TransactionCode = (function () {
    function TransactionCode(fb, navParams, viewCtrl) {
        this.fb = fb;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.error = "";
        this.message = "";
    }
    TransactionCode.prototype.ngOnInit = function () {
        this.transactioncode = this.fb.group({
            transactioncode: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].minLength(10), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].maxLength(10)]]
        });
    };
    TransactionCode.prototype.ionViewWillEnter = function () {
        this.error = this.navParams.get("error");
        this.message = this.navParams.get("message");
        document.getElementById('message').innerHTML = this.message;
        this.placeholder = this.navParams.get("placeholder");
        this.subtitle = this.navParams.get("subtitle");
        this.title = this.navParams.get("title");
    };
    TransactionCode.prototype.transact = function () {
        this.viewCtrl.dismiss(this.transactioncode.value);
    };
    TransactionCode.prototype.cancel = function () {
        this.viewCtrl.dismiss('cancelled');
    };
    return TransactionCode;
}());
TransactionCode = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'transactioncode',template:/*ion-inline-start:"/home/ubuntu/workspace/client/src/pages/transactioncode/transactioncode.html"*/'<ion-header>\n\n  <ion-toolbar>\n\n    <h5 class="lasso" text-center>{{title}}</h5>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <h5 text-center class="lasso">{{subtitle}}</h5>\n\n  <br />\n\n  <p [hidden]="!error" class="danger" text-center>{{error}}</p>\n\n  <br />\n\n  <div id="message" text-center></div>\n\n</ion-content>\n\n<ion-footer>\n\n  <ion-row>\n\n    <ion-col></ion-col>\n\n    <ion-col>\n\n      <button ion-button outline color="danger" (click)="cancel()">Cancel</button>\n\n    </ion-col>\n\n    <ion-col></ion-col>\n\n    <ion-col col-4>\n\n      <form novalidate [formGroup]="transactioncode">\n\n        <ion-list>\n\n          <ion-item>\n\n            <ion-label color="lasso" floating>{{placeholder}}</ion-label>\n\n            <ion-input type="string" clearInput formControlName="transactioncode"></ion-input>\n\n          </ion-item>\n\n        </ion-list>\n\n      </form>\n\n    </ion-col>\n\n    <ion-col></ion-col>\n\n    <ion-col>\n\n      <button ion-button block color="secondary" [disabled]="!transactioncode.valid" (click)="transact()">Confirm</button>\n\n    </ion-col>\n\n    <ion-col></ion-col>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"/home/ubuntu/workspace/client/src/pages/transactioncode/transactioncode.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ViewController */]])
], TransactionCode);

//# sourceMappingURL=transactioncode.js.map

/***/ })

});
//# sourceMappingURL=1.main.js.map