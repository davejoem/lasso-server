webpackJsonp([8],{

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InternetBundlesPageModule", function() { return InternetBundlesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__internet_bundles__ = __webpack_require__(765);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InternetBundlesPageModule = (function () {
    function InternetBundlesPageModule() {
    }
    return InternetBundlesPageModule;
}());
InternetBundlesPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__internet_bundles__["a" /* InternetBundles */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__internet_bundles__["a" /* InternetBundles */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__internet_bundles__["a" /* InternetBundles */]
        ]
    })
], InternetBundlesPageModule);

//# sourceMappingURL=internet-bundles.module.js.map

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InternetBundles; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_providers__ = __webpack_require__(421);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InternetBundles = (function () {
    function InternetBundles(user, viewCtrl) {
        this.user = user;
        this.viewCtrl = viewCtrl;
        this.bundle_type = 'speed';
    }
    InternetBundles.prototype.ngOnInit = function () {
        this.refresh();
        this.speedbundles = [
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                cost: 0.5,
                image: "assets/imgs/tortoise.png",
                name: 'Mini',
                profile: "512Kbps_Profile",
                speed: "512 KB/s",
                type: "speed"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                cost: 1,
                image: "assets/imgs/donkey.png",
                name: 'Medium',
                profile: "1Mbps_Profile",
                speed: "1 MB/s",
                type: "speed"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                cost: 2,
                image: "assets/imgs/horse.png",
                name: "Above Average",
                profile: "2Mbps_Profile",
                speed: "2 MB/s",
                type: "speed"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                cost: 3,
                image: "assets/imgs/cheetah.png",
                name: "Mega",
                profile: "3Mbps_Profile",
                speed: "3 MB/s",
                type: "speed"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                cost: 4,
                image: "assets/imgs/jet.png",
                name: "Super",
                profile: "4Mbps_Profile",
                speed: "4 MB/s",
                type: "speed"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                cost: 5,
                image: "assets/imgs/rocket.png",
                name: "Blast",
                profile: "5Mbps_Profile",
                speed: "5 MB/s",
                type: "speed"
            }
        ];
        this.sizebundles = [
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                bytes: 214748364,
                cost: 50,
                image: "assets/imgs/hare.png",
                name: 'Mini',
                price: 0.4,
                size: "200MB",
                type: "size"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                bytes: 284541583,
                cost: 75,
                image: "assets/imgs/antelope.jpg",
                name: 'Medium',
                price: 0.35,
                size: "265MB",
                type: "size"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                bytes: 322122547,
                cost: 100,
                image: "assets/imgs/buffalo.jpg",
                name: "Big",
                price: 0.30,
                size: "300MB",
                type: "size"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                bytes: 536870912,
                cost: 200,
                image: "assets/imgs/rhino.png",
                name: "Mega",
                price: 0.25,
                size: "500MB",
                type: "size"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                bytes: 1073741824,
                cost: 500,
                image: "assets/imgs/jumbo.png",
                name: "Jumbo",
                price: 0.20,
                size: "1GB",
                type: "size"
            },
            {
                brcolor: "#2ec95c",
                bgcolor: "#2ec95c",
                bytes: 2684354560,
                cost: 1000,
                image: "assets/imgs/whale.png",
                name: "Gagantua",
                price: 0.15,
                size: "2.5GB",
                type: "size"
            }
        ];
    };
    InternetBundles.prototype.chooseBundle = function (bundle) {
        this.viewCtrl.dismiss(bundle);
    };
    InternetBundles.prototype.explainLP = function () {
        this.viewCtrl.dismiss('lp');
    };
    InternetBundles.prototype.redeemLP = function () {
        this.viewCtrl.dismiss('redeem');
    };
    InternetBundles.prototype.refresh = function () {
        var _this = this;
        this.user.refresh(this.user._user.accounts.local.phonenumber).subscribe(function (res) {
            _this.user.save(res.user).then(function () {
                _this.balance = _this.user._user.finances.balance;
                _this.lp = _this.user._user.finances.lp;
            });
        });
    };
    return InternetBundles;
}());
InternetBundles = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'internet-bundles',template:/*ion-inline-start:"/home/ubuntu/workspace/client/src/pages/internet-bundles/internet-bundles.html"*/'<ion-header>\n\n  <ion-toolbar no-border-bottom>\n\n    <h5 class="lasso" text-center>Internet Bundles</h5>\n\n    <ion-buttons end>\n\n      <button ion-button clear small (click)="refresh">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <p>These bundles are packaged according to the speed or size they offer. Please choose one.</p>\n\n      <p>You currently have <i><b>Ksh. {{balance}}</b></i> and <i><b>{{lp}} LP</b></i> on your account.</p>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col></ion-col>\n\n      <ion-col>\n\n        <button class="lp" ion-button clear small (click)="explainLP()">What is LP ?</button>\n\n      </ion-col>\n\n      <ion-col *ngIf="lp > 0"></ion-col>\n\n      <ion-col *ngIf="lp > 0">\n\n        <button class="lp" ion-button clear small color="secondary" (click)="redeemLP()">Redeem LP</button>\n\n      </ion-col>\n\n      <ion-col></ion-col>\n\n    </ion-row>\n\n    <ion-row nowrap>\n\n      <ion-col col-6>\n\n        <h5 text-center class="lasso">Speed</h5>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <h5 text-center class="lasso">Size</h5>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row wrap>\n\n      <ion-col col-6>\n\n        <ion-card\n\n          class="bundle"\n\n          *ngFor="let bundle of speedbundles"\n\n          [style.backgroundColor]="bundle.bgcolor"\n\n          [style.borderColor]="bundle.brcolor"\n\n          (click)="chooseBundle(bundle)"\n\n        >\n\n          <ion-grid>\n\n            <img [src]="bundle.image" />\n\n            <ion-row class="bundle-name" text-center>{{bundle.name}}</ion-row>\n\n            <ion-row class="bundle-cost" text-center>Ksh. {{bundle.cost}} / Minute</ion-row>\n\n            <ion-row class="bundle-speed" text-center>{{bundle.speed}}</ion-row>\n\n          </ion-grid>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <ion-card\n\n          class="bundle"\n\n          *ngFor="let bundle of sizebundles"\n\n          [style.backgroundColor]="bundle.bgcolor"\n\n          [style.borderColor]="bundle.brcolor"\n\n          (click)="chooseBundle(bundle)"\n\n        >\n\n          <ion-grid>\n\n            <img [src]="bundle.image" />\n\n            <ion-row class="bundle-name" text-center>{{bundle.name}}</ion-row>\n\n            <ion-row class="bundle-cost" text-center>Ksh. {{bundle.cost}}</ion-row>\n\n            <ion-row class="bundle-size" text-center>{{bundle.size}}</ion-row>\n\n            <!--<ion-row class="bundle-price" text-center>Ksh. {{bundle.price}} / MB</ion-row>-->\n\n          </ion-grid>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/ubuntu/workspace/client/src/pages/internet-bundles/internet-bundles.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_providers__["b" /* User */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
], InternetBundles);

//# sourceMappingURL=internet-bundles.js.map

/***/ })

});
//# sourceMappingURL=8.main.js.map