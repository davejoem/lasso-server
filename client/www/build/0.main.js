webpackJsonp([0],{

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialModule", function() { return TutorialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__tutorial__ = __webpack_require__(773);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(146);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TutorialModule = (function () {
    function TutorialModule() {
    }
    return TutorialModule;
}());
TutorialModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [__WEBPACK_IMPORTED_MODULE_1__tutorial__["a" /* Tutorial */]],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_1__tutorial__["a" /* Tutorial */]),
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["a" /* TranslateModule */].forChild()
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_1__tutorial__["a" /* Tutorial */]
        ]
    })
], TutorialModule);

//# sourceMappingURL=tutorial.module.js.map

/***/ }),

/***/ 773:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tutorial; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(146);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Tutorial = (function () {
    function Tutorial(alertCtrl, loadingCtrl, menu, navCtrl, platform, translateService, user) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.translateService = translateService;
        this.user = user;
        this.activeindex = 0;
    }
    Tutorial.prototype.ngOnInit = function () {
        var _this = this;
        this.translateService.get([
            "PHONE_NUMBER_DESCRIPTION",
            "SIGNUP_ERROR",
            "TUTORIAL_SLIDE1_TITLE",
            "TUTORIAL_SLIDE1_DESCRIPTION",
            "TUTORIAL_SLIDE2_TITLE",
            "TUTORIAL_SLIDE2_DESCRIPTION",
            "TUTORIAL_SLIDE3_TITLE",
            "TUTORIAL_SLIDE3_DESCRIPTION",
        ]).subscribe(function (values) {
            _this.signupErrorString = values.SIGNUP_ERROR;
            _this.slides = [
                {
                    title: values.TUTORIAL_SLIDE1_TITLE,
                    description: values.TUTORIAL_SLIDE1_DESCRIPTION,
                    image: 'assets/imgs/ica-slidebox-img-1.png',
                },
                {
                    title: values.TUTORIAL_SLIDE3_TITLE,
                    description: values.TUTORIAL_SLIDE3_DESCRIPTION,
                    image: 'assets/imgs/ica-slidebox-img-3.png',
                }
            ];
            if (!_this.user._user) {
                _this.slides.splice(1, 0, {
                    title: values.TUTORIAL_SLIDE2_TITLE,
                    description: values.TUTORIAL_SLIDE2_DESCRIPTION,
                    image: 'assets/imgs/ica-slidebox-img-2.png',
                });
            }
        });
    };
    Tutorial.prototype.download = function () {
        if (this.platform.is('android')) {
            console.log("Downloading for android.");
        }
        if (!this.platform.is('mobile')) {
            console.log("Downloading for desktop.");
        }
    };
    Tutorial.prototype.ionViewDidEnter = function () {
        // the root left menu should be disabled on the tutorial page
        this.menu.enable(false);
        this.slider.enableKeyboardControl(true);
    };
    Tutorial.prototype.ionViewWillLeave = function () {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true);
    };
    Tutorial.prototype.nextSlide = function () {
        this.slider.slideNext();
        this.activeindex = this.slider._activeIndex;
    };
    Tutorial.prototype.onSlideChangeStart = function (slider) {
    };
    Tutorial.prototype.prevSlide = function () {
        this.slider.slidePrev();
        this.activeindex = this.slider._activeIndex;
    };
    Tutorial.prototype.sign = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: "Sign Up",
            message: "Enter your phone number. We will send you an activation code.",
            inputs: [
                {
                    name: "phonenumber",
                    placeholder: "Phone Number E.g. 07.."
                },
            ],
            buttons: [
                {
                    text: "Cancel",
                    role: 'cancel',
                    handler: function () {
                        // prompt.dismiss()
                    }
                },
                {
                    text: 'Sign Up',
                    handler: function (data) {
                        var loader = _this.loadingCtrl.create({
                            content: "Please wait..."
                        });
                        loader.present();
                        _this.user.signUp(data).subscribe(function (resp) {
                            loader.dismiss().then(function () {
                                var alert = _this.alertCtrl.create({
                                    title: "Sign Up",
                                    message: "Enter the activation code you recieved.",
                                    inputs: [
                                        {
                                            name: "activationcode",
                                            placeholder: "Activation Code"
                                        }
                                    ],
                                    buttons: [
                                        {
                                            text: "Cancel",
                                            handler: function () {
                                                alert.dismiss();
                                            }
                                        },
                                        {
                                            text: "Activate",
                                            handler: function (code) {
                                                _this.user.activate({
                                                    code: code,
                                                    user: _this.user._user
                                                }).subscribe(function (resp) {
                                                    _this.nextSlide();
                                                });
                                            }
                                        }
                                    ],
                                    enableBackdropDismiss: false
                                });
                                alert.present();
                            });
                        }, function (err) {
                            _this.alertCtrl.create({
                                title: "Sign Up Error",
                                message: _this.signupErrorString
                            }).present();
                        });
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        prompt.present();
    };
    Tutorial.prototype.slideChanged = function (slider) {
        this.activeindex = this.slider._activeIndex;
    };
    Tutorial.prototype.startApp = function () {
        this.navCtrl.setRoot('Home', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    return Tutorial;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Slides */])
], Tutorial.prototype, "slider", void 0);
Tutorial = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tutorial',template:/*ion-inline-start:"/home/ubuntu/workspace/client/src/pages/tutorial/tutorial.html"*/'<ion-content no-bounce>\n  <button id="skip_button" ion-button color="danger" clear [hidden]="activeindex == 3" (click)="startApp()">Skip</button>\n  <ion-slides pager="true" (ionWillChange)="onSlideChangeStart($event)" (ionSlideDidChange)="slideChanged($event)">\n    <ion-slide *ngFor="let slide of slides">\n      <img [src]="slide.image" [class.smaller]="activeindex == 1" class="slide-image" />\n      <h2 class="slide-title" [innerHTML]="slide.title"></h2>\n      <div [innerHTML]="slide.description"></div>\n      <button ion-button color="primary" *ngIf="activeindex == 2" (click)="sign()">\n        Sign Up\n      </button>\n      <button ion-button round color="secondary" *ngIf="activeindex == 1" (click)="download()">\n        <ion-icon name="download"></ion-icon>\n      </button>\n    </ion-slide>\n    <ion-slide>\n      <img src="assets/imgs/ica-slidebox-img-4.png" class="slide-image" />\n      <h2 class="slide-title">{{ \'TUTORIAL_SLIDE4_TITLE\' | translate }}</h2>\n      <button ion-button icon-right large clear (click)="startApp()">\n        {{ \'TUTORIAL_CONTINUE_BUTTON\' | translate }}\n        <ion-icon name="arrow-forward"></ion-icon>\n      </button>\n    </ion-slide>\n  </ion-slides>\n  <ion-fab left bottom (click)="prevSlide()" [hidden]="activeindex == 0">\n    <button ion-fab mini color="primary"><ion-icon name="arrow-back"></ion-icon></button>\n  </ion-fab>\n  <ion-fab right bottom (click)="nextSlide()" [hidden]="activeindex == 3">\n    <button ion-fab mini color="primary"><ion-icon name="arrow-forward"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/ubuntu/workspace/client/src/pages/tutorial/tutorial.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
        __WEBPACK_IMPORTED_MODULE_2__providers_user__["a" /* User */]])
], Tutorial);

//# sourceMappingURL=tutorial.js.map

/***/ })

});
//# sourceMappingURL=0.main.js.map