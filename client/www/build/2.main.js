webpackJsonp([2],{

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignModule", function() { return SignModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sign__ = __webpack_require__(771);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignModule = (function () {
    function SignModule() {
    }
    return SignModule;
}());
SignModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__sign__["a" /* Sign */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__sign__["a" /* Sign */]),
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* ReactiveFormsModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__sign__["a" /* Sign */]
        ]
    })
], SignModule);

//# sourceMappingURL=sign.module.js.map

/***/ }),

/***/ 771:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Sign; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Sign = (function () {
    function Sign(navCtrl, fb, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.error = "";
        this.message = "";
    }
    Sign.prototype.ngOnInit = function () {
        function validateNumber(c) {
            return c.value.startsWith('07') && parseInt(c.value) >= 700000000 && parseInt(c.value) < 800000000
                ? null
                : { validateNumber: { valid: false } };
        }
        this.phonenumber = this.fb.group({
            phonenumber: ['07', [validateNumber, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].minLength(10), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].maxLength(10), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]]
        });
    };
    Sign.prototype.ionViewWillEnter = function () {
        this.error = this.navParams.get("error");
        this.message = this.navParams.get("message");
    };
    Sign.prototype.sign = function () {
        this.viewCtrl.dismiss(this.phonenumber.value);
    };
    Sign.prototype.cancel = function () {
        this.viewCtrl.dismiss('cancelled');
    };
    return Sign;
}());
Sign = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sign',template:/*ion-inline-start:"/home/ubuntu/workspace/client/src/pages/sign/sign.html"*/'<ion-header>\n  <ion-toolbar>\n    <h5 class="lasso" text-center>Sign Up/ Sign In</h5>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <h5 text-center class="lasso">Enter your mobile phone number.</h5>\n  <br />\n  <p [hidden]="!error" class="danger" text-center>{{error}}</p>\n  <br />\n  <p text-center>{{message}}</p>\n  <ion-row>\n    <ion-col></ion-col>\n    <ion-col col-6>\n      <form novalidate [formGroup]="phonenumber">\n        <ion-list>\n          <ion-item>\n            <ion-label color="lasso" floating>Phone Number</ion-label>\n            <ion-input type="text" clearInput formControlName="phonenumber"></ion-input>\n          </ion-item>\n        </ion-list>\n      </form>\n    </ion-col>\n    <ion-col></ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <ion-row>\n    <ion-col></ion-col>\n    <ion-col>\n      <button ion-button outline color="danger" (click)="cancel()">Cancel</button>\n    </ion-col>\n    <ion-col></ion-col>\n    <ion-col>\n      <button ion-button block color="secondary" [disabled]="!phonenumber.valid" (click)="sign()">Submit</button>\n    </ion-col>\n    <ion-col></ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/home/ubuntu/workspace/client/src/pages/sign/sign.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ViewController */]])
], Sign);

//# sourceMappingURL=sign.js.map

/***/ })

});
//# sourceMappingURL=2.main.js.map