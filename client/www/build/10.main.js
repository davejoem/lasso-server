webpackJsonp([10],{

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CodeModule", function() { return CodeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__code__ = __webpack_require__(763);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CodeModule = (function () {
    function CodeModule() {
    }
    return CodeModule;
}());
CodeModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__code__["a" /* Code */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__code__["a" /* Code */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__code__["a" /* Code */]
        ]
    })
], CodeModule);

//# sourceMappingURL=code.module.js.map

/***/ }),

/***/ 763:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Code; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Code = (function () {
    function Code(navCtrl, fb, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.error = "";
        this.message = "";
    }
    Code.prototype.ngOnInit = function () {
        function validateCode(c) {
            return c.value <= 999999 && c.value > 0 && c.value.toString().length == 6
                ? null
                : { validateNumber: { valid: false } };
        }
        this.activationcode = this.fb.group({
            activationcode: ['', [validateCode, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]]
        });
    };
    Code.prototype.ionViewWillEnter = function () {
        this.error = this.navParams.get("error");
        this.message = this.navParams.get("message");
        this.placeholder = this.navParams.get("placeholder");
        this.subtitle = this.navParams.get("subtitle");
        this.title = this.navParams.get("title");
    };
    Code.prototype.activate = function () {
        this.viewCtrl.dismiss(this.activationcode.value);
    };
    Code.prototype.cancel = function () {
        this.viewCtrl.dismiss('cancelled');
    };
    Code.prototype.resend = function () {
        this.viewCtrl.dismiss('resend');
    };
    return Code;
}());
Code = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'activationcode',template:/*ion-inline-start:"/home/ubuntu/workspace/client/src/pages/code/code.html"*/'<ion-header>\n  <ion-toolbar>\n    <h5 class="lasso" text-center>{{title}}</h5>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <h5 text-center class="lasso">{{subtitle}}</h5>\n  <br />\n  <p [hidden]="!error" class="danger" text-center>{{error}}</p>\n  <br />\n  <p text-center><b>{{message}}</b></p>\n  <ion-row>\n    <ion-col></ion-col>\n    <ion-col col-6>\n      <form novalidate [formGroup]="activationcode">\n        <ion-list>\n          <ion-item>\n            <ion-label color="lasso" floating>{{placeholder}}</ion-label>\n            <ion-input type="number" clearInput formControlName="activationcode"></ion-input>\n          </ion-item>\n        </ion-list>\n      </form>\n    </ion-col>\n    <ion-col></ion-col>\n  </ion-row>\n</ion-content>\n<ion-footer>\n  <ion-row>\n    <ion-col></ion-col>\n    <ion-col>\n      <button ion-button outline color="danger" (click)="cancel()">Cancel</button>\n    </ion-col>\n    <ion-col></ion-col>\n    <ion-col>\n      <button ion-button block color="primary" (click)="resend()">Resend</button>\n    </ion-col>\n    <ion-col></ion-col>\n    <ion-col>\n      <button ion-button block color="secondary" [disabled]="!activationcode.valid" (click)="activate()">Actvate</button>\n    </ion-col>\n    <ion-col></ion-col>\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/home/ubuntu/workspace/client/src/pages/code/code.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ViewController */]])
], Code);

//# sourceMappingURL=code.js.map

/***/ })

});
//# sourceMappingURL=10.main.js.map