import { Injectable } from '@angular/core'
import { Storage } from '@ionic/storage'
import { Api } from './api'
import { HybridMessage } from '../models/hybrid-message'

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  private USER_KEY: string = '_user';
  public _user: any;

  constructor(
    public api: Api
    , public storage: Storage
  ) {}
  /**
   * Send a POST request to our activation endpoint with the data
   * the user entered on the form.
   */
  activate(code: any) {
    return this.api.send(new HybridMessage('/auth/user/activate', `post`, code))
  }
  /**
  * Send a POST request to our business buy endpoint with the user's session
  * and bundle chosen
  */
  buyBundle(details: { bundle: any, session: any }) {
    return this.api.send(
      new HybridMessage(`/business/bundles/buy`, `post`,  {
        bundle: details.bundle
        , phonenumber: this._user.accounts.local.phonenumber
        , session: details.session
      })
    )
  }
  /**
   * Send a POST request to our code generation endpoint with the data
   * the user entered on the form.
   */
  genCode(phonenumber: any) {
    return this.api.send(new HybridMessage('/auth/user/newcode', `post`, phonenumber))
  }
  
  /**
   * Fetch user from storage
   */
  load() {
    return this.storage.get(this.USER_KEY)
  }
  /**
   * Send a POST request to our user existance check endpoint with the data
   * the user entered on the form.
   */
  check(phonenumber: number) {
    return this.api.send(new HybridMessage('/auth/user/check', `post`, phonenumber))
  }
  
  /**
   * Send a POST request to our redeem LP endpoint
   */
  redeem(item) {
    return this.api.send(new HybridMessage('/business/lp/redeem', `post`, { phonenumber: this._user.accounts.local.phonenumber, item: item }))
  }
  
  /**
   * Send a GET request to our user end point to get new values
   */
  refresh(phonenumber?: any) {
    return this.api.send(new HybridMessage('/auth/user/refresh', `post`, phonenumber? { phonenumber: phonenumber } : { phonenumber: this._user.accounts.local.phonenumber }))
  }
  
  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  signIn(phonenumber: any) {
    return this.api.send(new HybridMessage('/auth/user/signin', `post`, phonenumber))
  }
  
  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signUp(phonenumber: any) {
    return this.api.send(new HybridMessage('/auth/user/signup', `post`, phonenumber))
  }

  /**
   * Sign the user out, which forgets the session
   */
  signOut() {
    this._user = null;
    return this.save()
  }

  /**
   * Process a signin/signup response to store user data
   */
  _signedIn(user) {
    this._user = user;
  }
  save(user?: any) {
    if (user) this._signedIn(user)
    return this.storage.set(this.USER_KEY, user ? user : this._user)
  }
  transact(code: string) {
    return this.api.send(new HybridMessage('/business/payments/recieved', `post`, {
      transactioncode: code
      , phonenumber: this._user.accounts.local.phonenumber
    }))
  }
}
