import { Injectable } from '@angular/core'
import { Http, RequestOptions, URLSearchParams } from '@angular/http'
import { Observable } from 'rxjs'
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/map'
import * as io from 'socket.io-client'
import { HybridMessage } from '../models/hybrid-message'
/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  socket: SocketIOClient.Socket
  auth_socket: SocketIOClient.Socket
  biz_socket: SocketIOClient.Socket
  general_socket: SocketIOClient.Socket
  radio_socket: SocketIOClient.Socket
  movie_socket: SocketIOClient.Socket
  music_socket: SocketIOClient.Socket
  telegram_socket: SocketIOClient.Socket
  
  constructor(public http: Http) {
  }
  
  connectSocket(domain?: string): Observable<SocketIOClient.Socket> {
    return Observable.create(observer => {
      if (!domain) {
        this.socket = io(this.url);
        observer.next(this.socket);
        observer.complete()
      } else {
        switch(domain) {
          case `auth`:
            this.auth_socket = io(this.url+'/auth');
            observer.next(this.socket);
            observer.complete()
            break
          case `biz`:
            this.biz_socket = io(this.url+'/business');
            observer.next(this.biz_socket);
            observer.complete()
            break
          case `general`:
            this.general_socket = io(this.url+'/general');
            observer.next(this.general_socket);
            observer.complete()
            break
          case `radio`:
            this.radio_socket = io(this.url+'/radio');
            observer.next(this.radio_socket);
            observer.complete()
            break 
          case `movie`:
            this.movie_socket = io(this.url+'/movie');
            observer.next(this.movie_socket);
            observer.complete()
            break
          case `music`:
            this.music_socket = io(this.url+'/music');
            observer.next(this.music_socket);
            observer.complete()
            break
          case `telegram`:
            this.telegram_socket = io(this.url+'/telegram');
            observer.next(this.telegram_socket);
            observer.complete()
            break
        }
      }
    })
  }
  disconnectSocket(): Observable<SocketIOClient.Socket> {
    return Observable.create(observer => {
      this.socket.disconnect();
      observer.next(this.socket);
      observer.complete()
    })
  }
  
  get url(): string {
  	this.send(new HybridMessage(`/socket`, `get`)).subscribe(
  		res =>	return res.address  		
  		, err => return location.href
  	)
  }
  
  socketConnected(): boolean {
    return this.socket.connected
  }
  
  send(message: HybridMessage): Observable<any> {
    // if (this.socket.connected) {
    //   return Observable.create(observer=>{
    //     this.socket.emit(message.message, message.data, (err, res) => {
    //       if (err) observer.error(err)
    //       else observer.next(JSON.parse(res))
    //       observer.complete()
    //     })
    //   })
    // }
    // else {
      switch(message.method) {
        case 'delete':
          return this.delete(message.url, message.opts)
                     .map(res => res.json())
                     .catch(err => err.json())
        case 'get': 
          return this.get(message.url, message.params, message.opts)
                     .map(res => res.json())
                     .catch(err => err.json())
        case 'patch':
          return this.patch(message.url, message.data, message.opts)
                     .map(res => res.json())
                     .catch(err => err.json())
        case 'post':
          return this.post(message.url, message.data, message.opts)
                     .map(res => res.json())
                     .catch(err => err.json())
        case 'put':
          return this.put(message.url, message.data, message.opts)
                     .map(res => res.json())
                     .catch(err => err.json())
      }
    // }
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + endpoint, options);
  }
  
  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }
      // Set the search field if we have params and don't already have
      // a search field set in options.
      options.search = !options.search && p || options.search;
    }

    return this.http.get(this.url + endpoint, options);
  }
  
  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + endpoint, body, options);
  }
  
  post(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.post(this.url + endpoint, body, options);
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + endpoint, body, options);
  }
}