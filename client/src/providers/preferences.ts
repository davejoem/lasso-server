import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
@Injectable()
export class Preferences {
  private PREFERENCES_KEY: string = '_preferences';

  preferences: any;

  _defaults: any;
  _readyPromise: Promise<any>;

  constructor(
    public storage: Storage
    , defaults?: any
  ) {
    if (defaults) this._defaults = defaults;
  }

  load() {
    return this.storage.get(this.PREFERENCES_KEY).then((value) => {
      if (value) {
        this.preferences = value;
        this._mergeDefaults(this._defaults);
      } else {
        return this.setAll(this._defaults).then((val) => {
          this.preferences = val;
        })
      }
    });
  }

  _mergeDefaults(defaults: any) {
    for (let k in defaults) {
      if (!(k in this.preferences)) {
        this.preferences[k] = defaults[k];
      }
    }
    return this.setAll(this.preferences);
  }

  merge(preferences: any) {
    for (let k in preferences) {
      this.preferences[k] = preferences[k];
    }
    return this.save();
  }

  setValue(key: string, value: any) {
    this.preferences[key] = value;
    return this.storage.set(this.PREFERENCES_KEY, this.preferences);
  }

  setAll(value: any) {
    return this.storage.set(this.PREFERENCES_KEY, value);
  }

  getValue(key: string) {
    return this.storage.get(this.PREFERENCES_KEY)
      .then(settings => {
        return settings[key];
      });
  }

  save() {
    return this.setAll(this.preferences);
  }

  get allSettings() {
    return this.preferences;
  }
}
