import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Alert, AlertController, Loading, LoadingController, Modal, ModalController, Platform, Nav, Config } from 'ionic-angular';
import { Observable } from 'rxjs'
import 'rxjs/add/operator/retry'
import { HybridMessage } from '../models/hybrid-message'
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';

import { Api, Preferences, User } from '../providers/providers'

import { TranslateService } from '@ngx-translate/core'

@Component({
  templateUrl: `app.html`
})
export class Lasso implements OnDestroy, OnInit {
  @ViewChild(Nav) nav: Nav;
  public code_alert: Alert
  public connected_alert: Alert
  public debit_alert: Alert
  public dev_expanded: boolean = false
  public internet_unavailable_alert: Alert
  public lasso_point_alert: Alert
  public loading: Loading
  public login_page: Window
  public menu: {
    side: string
    type: string
  }
  public nointernet_alert: Alert
  public rootPage: any
  public session: {
    details: {
      domain?: string
      dst?: string
      host_ip?: string
      interface?: string
      ip?: string
      logged_in?: string
      mac?: string
      username?: string
      from?: string
      trial?: string
    }
    status: {
      paused?: boolean
      speed?: any
      stopped?: boolean
      timeleft?: any
      type?: any
    }
  }
  public session_expanded: boolean = false
  public sign_alert: Alert
  public socket: SocketIOClient.Socket
  public transaction_alert: Alert
  public welcome_alert: Alert

  constructor(
    private alertCtrl: AlertController
    , private api: Api
    , private config: Config
    , private loadingCtrl: LoadingController
    , private modalCtrl: ModalController
    , private translate: TranslateService
    , private platform: Platform
    , private preferences: Preferences
    , private user: User
    // , statusBar: StatusBar
    // , splashScreen: SplashScreen
  ) {
    this.initTranslate()
    this.menu = {
      side: `right`
      , type: `overlay`
    }
    this.session = {
      details: {}
      , status: {
        paused: false
        , speed: ``
        , type: ``
      }
    }
  }
  ngOnInit() {
    let load = () => {
      this.user.load().then(user => {
        if (user != null) {
          this.user._user = user;
          this.user.refresh(user.accounts.local.phonenumber).subscribe(
            res => this.user.save(res.user).then(() => this.chooseLaunchPoint())
            , err => this.chooseLaunchPoint()
          )
        }
        else
          this.chooseLaunchPoint()
      })
    }
    this.platform.ready().then(() => {
      this.api.connectSocket().subscribe(
        socket => {
          load()
        }
        , err => {
          console.log(err)
          load()
        }
      )
    })
  }
  ngOnDestroy() {
    this.api.disconnectSocket()
  }
  buyBundle(bundle) {
    let l = this.loadingCtrl.create({ content: `Buying, please wait ...` })
    l.present().then(() =>
      this.user.buyBundle({ bundle: bundle, session: this.session.details }).subscribe(
        res => l.dismiss().then(() => this.showConnectedAlert({ username: res.username, password: res.password }))
        , err => {
          console.log(err)
          l.dismiss().then(() => {
            let e = this.alertCtrl.create({
              title: `<p class="lasso">Couldn't buy this bundle</p>`
              , subTitle: `<p class="danger">An error occured</p>`
              , message: err.message
              , buttons: [
                {
                  text: `Try again`,
                  handler: () => {
                    e.dismiss().then(() => this.showBundlesModal())
                    return false
                  }
                }
              ]
            })
            e.present()
          })
        }
      )
    )
  }
  chooseLaunchPoint() {
    if (document.referrer.includes("127.0.0.1") || document.referrer.includes("lasso.network")) {
      this.api.send(new HybridMessage('/admin/service/available', `get`)).retry(3).subscribe(
        res => {
          // if (res.available) {
          //     let ask = this.loadingCtrl.create({
          //       content: 'Requesting device details ...'
          //     })
          //     ask.present().then(()=>{
          //       this.startComms('session:details:send').subscribe(
          //         session=>{
          //           this.session.details = session
          //           ask.dismiss().then(()=>this.showWelcomeAlert())
          //         }
          //         , err=>this.rootPage = `Tutorial`
          //       )
          //     })
          // }
          // else
          //   this.showInternetUnavailableAlert()
          this.showWelcomeAlert()
        }
        , err => this.showInternetUnavailableAlert()
      )
    }
    else
      this.showWelcomeAlert()
      // this.rootPage = `Tutorial`
  }
  chooseMessage(messagetype: string, opt?: any, modal?: Alert | Modal): string {
    switch (messagetype) {
      case 'sign':
        if (opt) {
          return `
            <p>An error occured.</p>
            <p>Please enter your phone number again.</p>
            <p>Format: 07......</p>`
        } else {
          return `<p>You are not signed in</p>
          <p>Please enter your phone number</p>
          <p>Format: 07......</p>`
        }
    }
  }
  endSession() {
    top.postMessage('session:end', document.referrer.split('?')[0])
    window.addEventListener("message", event => {
      console.log(event)
      if (!event.origin.includes(document.referrer.split('?')[0]))
        return;
      if (event.data.event == "session:status:ended") {
        top.postMessage('session:status:recieved', document.referrer.split('?')[0])
      }
    })
  }
  explainLassoPoint(alert?: Alert): void {
    this.lasso_point_alert = this.alertCtrl.create({
      title: '<p class="lasso">Lasso Point</p>'
      , subTitle: 'What is it?'
      , message: `
        <p>A Lasso Point <i>(LP)</i> is a bonus given to you based on your expenditure on the Lasso Network.
        You get <b>one LP</b> for every <b>Ksh. 10</b> you spend on the Lasso Network</p>
        <p>You can redeem you <b>LP</b> to purchase content the Lasso Network.</p>
        <p>Its <b>value</b> is equal to <b>one shilling</b>.</p>
      `
      , buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            this.lasso_point_alert.dismiss().then(() => { alert ? alert.present() : this.showBundlesModal() })
            return false
          }
        }
      ]
      , enableBackdropDismiss: false
    })
    this.lasso_point_alert.present()
  }
  goToTutorial() {
    this.nav.push('Tutorial')
  }
  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }
  informOfNoInternet() {
    this.nointernet_alert = this.alertCtrl.create({
      message: `You will not have access to the internet and anything outside the Lasso network.`
      , buttons: [{
        text: 'Ok',
        handler: () => {
          this.nointernet_alert.dismiss().then(() => this.rootPage = `Tutorial`)
          return false
        }
      }]
      , enableBackdropDismiss: false
    })
    this.nointernet_alert.present()
  }
  toggleSession() {
    this.session.status.paused = !this.session.status.paused
  }
  showBundlesModal() {
    let modal = this.modalCtrl.create(`InternetBundles`, { enableBackdropDismiss: false })
    modal.onDidDismiss(bundle => {
      if (bundle == `lp`)
        return this.explainLassoPoint()
      if (bundle == `redeem`)
        return this.showRedeemLPModal()
      if (bundle.cost > this.user._user.finances.balance)
        this.showTransactionCodeModal(bundle, false)
      else
        this.showConfirmBuyAlert(bundle)
    })
    modal.present()
  }
  showCodeModal(phonenumber: number, existed: boolean, error?: any) {
    let modal = this.modalCtrl.create(`Code`
      , {
        error: error ? error : null
        , title: existed ? 'Verify Sign In' : 'Activate Account'
        , subtitle: existed ? `Welcome back` : `Account ${phonenumber} created.`
        , message: `Please enter the code that was just sent to your number.`
        , placeholder: existed ? 'Confirmation Code' : 'Activation Code'
      }
      , { enableBackdropDismiss: false }
    )
    modal.onDidDismiss(data => {
      if (data == `cancelled`)
        return this.showSignModal(false)
      if (data == `resend`) {
        this.loading = this.loadingCtrl.create({ content: 'Requesting new code ...' })
        this.loading.present().then(() =>
          this.user.genCode({ phonenumber: phonenumber }).subscribe(
            res => this.loading.dismiss().then(() => this.showCodeModal(phonenumber, existed))
            , err => this.loading.dismiss().then(() => this.showCodeModal(phonenumber, existed, err.message))
          )
        )
        return
      }
      if (data.activationcode) {
        this.loading = this.loadingCtrl.create({ content: 'Activating ...' })
        this.loading.present().then(() =>
          this.user.activate({ phonenumber: phonenumber, activationcode: data.activationcode }).subscribe(
            res => this.loading.dismiss().then(() => this.user.save(res.user).then(() => this.showBundlesModal()))
            , err => this.loading.dismiss().then(() => this.showCodeModal(phonenumber, existed, err.message))
          )
        )
      }
    })
    modal.present()
  }
  showConfirmBuyAlert(bundle) {
    this.debit_alert = this.alertCtrl.create({
      title: '<p class="lasso">Confirm Bundle purchase</p>'
      , message: `
        <p>You currently have <b>Ksh. ${this.user._user.finances.balance}</b> and <b><a href="#" (click)="explainLassoPoint(debit_alert)">${this.user._user.finances.lp} LP</a></b> on your account.</p>
        <p>Your account will be debited Kshs. ${bundle.cost} ${bundle.type == 'speed' ? '<i> / Min</i>' : ''}.</p>
        ${ bundle.type == 'speed'
        ? '<p>You therefore can browse for about ' + Math.floor(this.user._user.finances.lp / parseFloat(bundle.cost)) + ' minutes on this bundle.</p>'
        : '<p>You will have ' + bundle.size + ' to use.</p>'
      }
        <p><b>Buy ${bundle.name} bundle?</b></p>
      `
      , buttons: [
        {
          text: `Change`,
          handler: () => {
            this.debit_alert.dismiss().then(() => this.showBundlesModal())
            return false
          }
        }
        , {
          text: `Yes`,
          handler: () => {
            this.debit_alert.dismiss().then(() => {
              if (bundle.cost > (this.user._user.finances.balance + this.user._user.finances.lp))
                this.showTransactionCodeModal(bundle, false)
              else
                this.buyBundle(bundle)
            })
            return false
          }
        }
      ]
      , enableBackdropDismiss: false
    })
    this.debit_alert.present()
  }
  showConnectedAlert(credentials) {
    this.connected_alert = this.alertCtrl.create({
      title: '<p class="lasso">Internet Access Granted</p>'
      , subTitle: 'You now have internet connectivity.'
      , message: `
          <p>Internet connectivity is provided by <a href="https://davejoem.github.io/rhedon/">Rhedon Mega Cyber.</p>
          <br />
          <p><b>Where to next?</b></p>
        `
      , buttons: [
        {
          text: 'Lasso Network'
          , handler: () => {
            this.connected_alert.dismiss().then(() => {
              top.postMessage({
                event: "session:credentials"
                , data: credentials
                , dstpath: this.api.url
              }, document.referrer.split('?')[0]);
              this.rootPage = `Tutorial`
            })
            return false
          }
        },
        {
          text: this.session.details.dst
          , handler: () => {
            this.connected_alert.dismiss().then(() => {
              top.postMessage({
                event: "session:credentials"
                , data: credentials
                , dstpath: this.session.details.dst
              }, document.referrer.split('?')[0]);
            })
            return false
          }
        }
      ]
      , enableBackdropDismiss: false
    })
    this.connected_alert.present()
  }
  showInternetUnavailableAlert() {
    this.internet_unavailable_alert = this.alertCtrl.create({
      title: `<p class="lasso">Internet Service</p>`
      , subTitle: `<p class="danger">Unavailable!</p>`
      , message: `<p>We regret to inform you that internet services are currently unavailable.
                  <p>You may continue to Lasso Network for other services that are still available.</p>`
      , buttons: [
        {
          text: `Ok`
          , handler: () => {
            this.internet_unavailable_alert.dismiss().then(() => this.rootPage = `Tutorial`)
            return false
          }
        }
      ]
      , enableBackdropDismiss: false
    })
    this.internet_unavailable_alert.present()
  }
  showRedeemLPModal() {
    let modal = this.modalCtrl.create(`RedeemLP`, { enableBackdropDismiss: false })
    modal.onDidDismiss(data => {
      if (data == 'cancelled')
        return this.showBundlesModal()
      this.user.redeem(data)
    })
    modal.present()
  }
  showSignModal(has_error: boolean, error?: any): void {
    let modal = this.modalCtrl.create(`Sign`
      , {
        message: has_error
          ? 'An error occured. Please enter your phone number again. Format: 07......'
          : 'You are not signed in. Please enter your phone number. Format: 07......'
        , error: error ? error : null
      }
      , { enableBackdropDismiss: false }
    )
    modal.onDidDismiss(data => {
      if (data == `cancelled`)
        return this.showWelcomeAlert()
      if (data.phonenumber) {
        this.loading = this.loadingCtrl.create({
          content: `Checking ...`
        })
        this.loading.present().then(() =>
          this.user.check(data).subscribe(
            (res: { status: string, message?: string, user?: any }) => {
              switch (res.status) {
                // If the API returned !exists response, then user doesn't exists
                case `!exists`:
                  this.loading.dismiss().then(() => {
                    this.loading = this.loadingCtrl.create({
                      content: `Creating account ...`
                    })
                    this.loading.present().then(() =>
                      this.user.signUp(data).subscribe(
                        res => this.loading.dismiss().then(() => this.showCodeModal(data.phonenumber, false))
                        , err => this.loading.dismiss().then(() => this.showSignModal(true, err.message))
                      )
                    )
                  })
                  break
                // If the API returned exists response, then user exists
                case `active`:
                  this.loading.dismiss().then(() => {
                    this.loading = this.loadingCtrl.create({
                      content: `Generating new code ...`
                    })
                    this.loading.present().then(
                      () => this.user.genCode(data).subscribe(
                        res => this.loading.dismiss().then(() => this.showCodeModal(data.phonenumber, true))
                        , err => this.loading.dismiss().then(() => this.showSignModal(true, err.message))
                      )
                    )
                  })
                  break
                case `error`:
                  this.loading.dismiss().then(() => this.showSignModal(true, res.message));
                  break
              }
            }
            , err => this.loading.dismiss().then(() => this.showSignModal(true, err.message))
          )
        )
      }
    })
    modal.present()
  }
  showTransactionCodeModal(bundle: any, error: boolean, used?: boolean) {
    let modal = this.modalCtrl.create(`TransactionCode`
      , {
        error: error ? error : null
        , title: `Insufficient Balance`
        , subtitle: `Your current balance is Ksh. ${this.user._user.finances.balance} and you have ${this.user._user.finances.lp} LP.`
        , message: !error
          ? `
            <p>Your do not have enough balance on your account <b>0${this.user._user.accounts.local.phonenumber}</b> to purchase the <b>${bundle.name}</b> bundle.</p>
            <p>Please to up your account to purchase this bundle.</p>
            <h5>Processs</h5>
            <ul>
              <li>From your M-PESA menu, Select <b>Lipa Na M-PESA</b></li>
              <li>Select <b>Buy Goods and Services</b></li>
              <li>Enter Till No.: <b>563368</b></li>
              <li>Enter the <b>amount</b> you want to send. (<small>This will be saved to your Lasso account.
                And you can use it buy anything on the Lasso Network</small>)
              </li>
              <li>Enter your PIN and send.</li>
            </ul>
            <p><b>Enter the <b>transaction code</b> that will be in the transaction confirmation message. We will also send the code directly to your number.</b></p>
          `
          : used
            ? `<p>This transaction code has already been used. Please enter another code.</p>`
            : `
              <p>An error occured when confirming the transaction.</p>
              <p>Please make sure you sent money via <b>Lipa Na MPESA > Buy Goods And Services</b> to Till No.: <b>563368</b>.
              <p>Please re-enter <b>ONLY</b> the <b>transaction code</b> that was included in a confirmation message sent to your number after you transacted.</b></p>
            `
        , placeholder: 'Transaction Code'
      }
      , { enableBackdropDismiss: false }
    )
    modal.onDidDismiss(data => {
      if (data == `cancelled`)
        return this.showBundlesModal()
      if (data.transactioncode) {
        this.loading = this.loadingCtrl.create({ content: 'Checking ...' })
        this.loading.present().then(() =>
          this.user.transact(data.transactioncode).subscribe(
            res => {
              switch (res.status) {
                case 'success':
                  this.loading.dismiss().then(() => {
                    this.loading = this.loadingCtrl.create({ content: 'Saving ...' })
                    this.loading.present().then(() => this.user.save(res.user).then(() => this.loading.dismiss().then(() => this.showBundlesModal())))
                  })
                  break
                case 'used':
                  this.loading.dismiss().then(() => this.showTransactionCodeModal(bundle, true, true))
                  break
              }
            }
            , () => this.loading.dismiss().then(() => this.showTransactionCodeModal(bundle, true))
          )
        )
      }
    })
    modal.present()
  }
  showWelcomeAlert() {
    this.welcome_alert = this.alertCtrl.create({
      title: '<p class="lasso">Welcome to the Lasso Network</p>'
      , subTitle: 'Internet access'
      , message: `
        <p>Connecting to this network does not guarantee you internet connectivity as it is a service you have to pay for.</p>
        <p>You can, however, do a lot on this network even without internet access.</p>
        <p><b>Do you want internet access?</b></p>`
      , buttons: [
        {
          text: 'No'
          , role: 'cancel'
          , cssClass: 'danger'
          , handler: () => {
            this.welcome_alert.dismiss().then(() => this.informOfNoInternet())
            return false
          }
        },
        {
          text: 'Yes',
          cssClass: 'lasso',
          handler: () => {
            this.welcome_alert.dismiss().then(() => {
              if (this.user._user != null)
                this.showBundlesModal()
              else
                this.showSignModal(false)
            })
            return false
          }
        }
      ]
      , enableBackdropDismiss: false
    })
    this.welcome_alert.present()
  }
  signOut() {
    this.user.signOut()
  }
  startComms(message?: string, data?: any): Observable<{}> {
    return Observable.create(observer => {
      if (message) top.postMessage({ event: message, data: data }, document.referrer.split('?')[0])
      window.addEventListener("message", event => {
        document['ev'] = event
        // if (!event.origin.includes('lasso.network') || !event.origin.includes('10.0.0.1') || !event.origin.includes('127.0.0.1') || !event.origin.includes('localhost'))
        //   return
        switch (event.data.event) {
          case 'session:details':
            // event.data should match
            // {
            // event: "device details"
            // data: {
            // domain: string
            // dst: string
            // , host_ip: string
            // , interface: string
            // , ip: string
            // , logged_in: string
            // , mac: string
            // , username: string
            // , from: string
            // , trial: string
            // }
            // } 
            if (!event.data.data.ip || !event.data.data.mac) {
              top.postMessage({ event: 'session:details:improper' }, event.origin)
            } else {
              this.login_page = event.source
              top.postMessage({ event: "session:details:recieved" }, event.origin);
              observer.next(event.data.data)
            }
            break
          case 'session:status:update':
            this.session.status = event.data.data
            break
          case 'session:credentials:recieved':
            top.postMessage({ event: "session:start" }, event.origin);
            break
        }
      })
    })
  }
}
