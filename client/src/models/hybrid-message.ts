import { RequestOptions } from '@angular/http'

export interface HybridMessageInterface {
  data?: any
  message: string
  method: string
  opts?: RequestOptions
  params?: any
  url: string
}

export class HybridMessage implements HybridMessageInterface {
  public data: any
  public message: string
  public method: string
  public opts: RequestOptions
  public params?: any
  public url: string
  constructor(
    target: string
    , method: string
    , data?: any
    , params?: any
    , opts?: RequestOptions
  ) {
    let hm
    function processTarget(ret) {
      switch(ret) {
        case 'url':
          if (target.startsWith('/')) {
            return target
          }
          else {
            target = `/${target.split(':')[0]}/${target.split(':')[1]}/${target.split(':')[2]}`
            return target
          }
        case 'endpoint':
          if (target.startsWith('/')) {
            target = `${target.split('/')[1]}:${target.split('/')[2]}:${target.split('/')[3]}`
            return target
          }
          else {
            return target
          }
      }
    }
    hm = {
      data: data
      , message: processTarget('endpoint')
      , method: method
      , opts: opts
      , params: params
      , url: processTarget('url')
    }
    return hm
  }
}