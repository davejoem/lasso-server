import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'home',
  templateUrl: 'home.html',
})
export class Home {
  public apps: any[]
  constructor(
    public navCtrl: NavController
    , public navParams: NavParams
    , private translateService: TranslateService
  ) {
    // this.translateService.get([]).subscribe(values=>{})
  }
  ngOnInit() {
    this.apps = [
      {
        name: 'Movies'
        , page: 'Movies'
        , icon: 'assets/imgs/movies.png'
      }
      , {
        name: 'Music'
        , page: 'Music'
        , icon: 'assets/imgs/music.png'
      }
      , {
        name: 'Radio'
        , page: 'Radio'
        , icon: 'assets/imgs/radio.png'
      }  
    ]
  }
  goToApp(page: string) {
    this.navCtrl.setRoot(page);
  }
}
