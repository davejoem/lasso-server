import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'sign',
  templateUrl: 'sign.html',
})
export class Sign implements OnInit {
  public error: string
  public message: string
  private phonenumber: FormGroup
  constructor(
    private navCtrl: NavController
    , private fb: FormBuilder
    , private navParams: NavParams
    , private viewCtrl: ViewController
  ) {
    this.error = ``
    this.message = ``
  }
  ngOnInit() {
    function validateNumber(c: FormControl) {
      return c.value.startsWith('07') && parseInt(c.value) >= 700000000 && parseInt(c.value) < 800000000
        ? null
        : { validateNumber: { valid: false }}
    }
    this.phonenumber = this.fb.group({
      phonenumber: ['07', [ validateNumber, Validators.minLength(10), Validators.maxLength(10), Validators.required ]]
    });
  }
  ionViewWillEnter() {
    this.error = this.navParams.get(`error`)
    this.message = this.navParams.get(`message`)
  }
  sign() {
    this.viewCtrl.dismiss(this.phonenumber.value)
  }
  cancel() {
    this.viewCtrl.dismiss('cancelled')
  }
}
