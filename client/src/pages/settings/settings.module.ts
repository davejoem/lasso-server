import { NgModule } from '@angular/core'
import { Settings } from './settings'
import { IonicPageModule } from 'ionic-angular'
import {TranslateModule} from '@ngx-translate/core'
 
@NgModule({
  declarations: [Settings],
  imports: [
    IonicPageModule.forChild(Settings)
    , TranslateModule.forChild()
  ]
})
export class SettingsModule { }
