import { Component } from '@angular/core'
import { IonicPage, MenuController, NavController, NavParams } from 'ionic-angular'
import { Api, User } from '../../providers/providers'
import { HybridMessage } from '../../models/hybrid-message'

@IonicPage()
@Component({
  selector: 'music',
  templateUrl: 'music.html',
})
export class Music {
  public albums
  public artists
  public tracks
  constructor(
    private api: Api
    , private menuCtrl: MenuController
    , public navCtrl: NavController
    , public navParams: NavParams
    , private user: User
  ) {
    this.artists = []
    this.albums = []
    this.tracks = []
  }
  goHome() {
    this.navCtrl.setRoot('Home')
  }
  refresh() {
    this.api.send(new HybridMessage('/music/albums/all', `get`)).subscribe(
      albums=>{
        this.albums = albums
      }
    )
    this.api.send(new HybridMessage('/music/artists/all', `get`)).subscribe(
      artists=>{
        this.artists = artists
      }
    )
    this.api.send(new HybridMessage('/music/songs/all', `get`)).subscribe(
      tracks=>{
        this.tracks = tracks
      }
    )
  }
  showMenu(side?: string) {
    this.menuCtrl.toggle(side ? side : 'right')
  }
}
