import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Music } from './music';

@NgModule({
  declarations: [
    Music,
  ],
  imports: [
    IonicPageModule.forChild(Music),
  ],
  exports: [
    Music
  ]
})
export class MusicModule {}
