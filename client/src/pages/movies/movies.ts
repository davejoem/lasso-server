import { Component, OnInit } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams } from 'ionic-angular';
import { Api, User } from '../../providers/providers'
import { HybridMessage } from '../../models/hybrid-message'

@IonicPage()
@Component({
  selector: 'movies',
  templateUrl: 'movies.html',
})
export class Movies implements OnInit {
  public movies
  constructor(
    private api: Api
    , private menuCtrl: MenuController
    , private navCtrl: NavController
    , private navParams: NavParams
    , private user: User
  ) {
    this.movies = []
  }
  
  ngOnInit() {
    
  }
  goHome() {
    this.navCtrl.setRoot('Home')
  }
  ionViewWillEnter() {
    this.refresh()
  }
  refresh() {
    this.api.send(new HybridMessage('/movies/list/all', `get`)).subscribe(
      movies=>{
        this.movies = movies
      }
    )
  }
  showMenu(side?: string) {
    this.menuCtrl.toggle(side ? side : 'right')
  }
}
