import { Component } from '@angular/core'
import { IonicPage, MenuController, NavController, NavParams } from 'ionic-angular'
import { Api, User } from '../../providers/providers'
import { HybridMessage } from '../../models/hybrid-message'

@IonicPage()
@Component({
  selector: 'radio',
  templateUrl: 'radio.html',
})
export class Radio {
  public stations
  constructor(
    private api: Api
    , private menuCtrl: MenuController
    , public navCtrl: NavController
    , public navParams: NavParams
    , private user: User
  ) {
    this.stations = []
  }
  goHome() {
    this.navCtrl.setRoot('Home')
  }
  refresh() {
    this.api.send(new HybridMessage('/radio/stations', `get`)).subscribe(
      stations=>{
        this.stations = stations
      }
    )
  }
  showMenu(side?: string) {
    this.menuCtrl.toggle(side ? side : 'right')
  }
}
