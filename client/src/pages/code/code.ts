import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'activationcode',
  templateUrl: 'code.html',
})
export class Code implements OnInit {
  public error: string
  public message: string
  public placeholder: string
  public subtitle: string
  public title: string
  private activationcode: FormGroup
  constructor(
    private navCtrl: NavController
    , private fb: FormBuilder
    , private navParams: NavParams
    , private viewCtrl: ViewController
  ) {
    this.error = ``
    this.message = ``
  }
  ngOnInit() {
    function validateCode(c: FormControl) {
      return c.value <= 999999 && c.value > 0 && c.value.toString().length == 6
        ? null
        : { validateNumber: { valid: false }}
    }
    this.activationcode = this.fb.group({
      activationcode: ['', [ validateCode, Validators.required ]]
    });
  }
  ionViewWillEnter() {
    this.error = this.navParams.get(`error`)
    this.message = this.navParams.get(`message`)
    this.placeholder = this.navParams.get(`placeholder`)
    this.subtitle = this.navParams.get(`subtitle`)
    this.title = this.navParams.get(`title`)
  }
  activate() {
    this.viewCtrl.dismiss(this.activationcode.value)
  }
  cancel() {
    this.viewCtrl.dismiss('cancelled')
  }
  resend() {
    this.viewCtrl.dismiss('resend')
  }
}
