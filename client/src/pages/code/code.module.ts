import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Code } from './code';

@NgModule({
  declarations: [
    Code,
  ],
  imports: [
    IonicPageModule.forChild(Code),
  ],
  exports: [
    Code
  ]
})
export class CodeModule {}
