import { Component, OnInit } from '@angular/core'
import { IonicPage, ViewController } from 'ionic-angular'
import { User } from '../../providers/providers'

@IonicPage()
@Component({
  selector: 'internet-bundles',
  templateUrl: 'internet-bundles.html'
})
export class InternetBundles implements OnInit {
  public bundle_type: string = 'speed'
  public balance: number
  public lp: number
  public speedbundles: Array<{
    brcolor: string
    bgcolor: string
    cost: number
    image: string
    name: string
    profile: string
    speed: string
    type: string
  }>
  public sizebundles: Array<{
    brcolor: string
    bgcolor: string
    bytes: number
    cost: number
    image: string
    name: string
    price: number
    size: string
    type: string
  }>
  
  constructor(
    private user: User
    , private viewCtrl: ViewController
  ) {}
  
  ngOnInit() {
    this.refresh()
    this.speedbundles = [
      { 
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , cost: 0.5
        
        , image: `assets/imgs/tortoise.png`
        , name: 'Mini'
        , profile: `512Kbps_Profile`
        , speed: `512 KB/s`
        , type: `speed`
      },
      {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , cost: 1
        
        , image: `assets/imgs/donkey.png`
        , name: 'Medium'
        , profile: `1Mbps_Profile`
        , speed: `1 MB/s`
        , type: `speed`
      }
      , {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , cost: 2
        
        , image: `assets/imgs/horse.png`
        , name: `Above Average`
        , profile: `2Mbps_Profile`
        , speed: `2 MB/s`
        , type: `speed`
      }
      , {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , cost: 3
        , image: `assets/imgs/cheetah.png`
        , name: `Mega`
        , profile: `3Mbps_Profile`
        , speed: `3 MB/s`
        , type: `speed`
      }
      , {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , cost: 4
        , image: `assets/imgs/jet.png`
        , name: `Super`
        , profile: `4Mbps_Profile`
        , speed: `4 MB/s`
        , type: `speed`
      }
      , {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , cost: 5
        , image: `assets/imgs/rocket.png`
        , name: `Blast`
        , profile: `5Mbps_Profile`
        , speed: `5 MB/s`
        , type: `speed`
      }
    ]
    this.sizebundles = [
      { 
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , bytes: 214748364
        , cost: 50
        , image: `assets/imgs/hare.png`
        , name: 'Mini'
        , price: 0.4
        , size: `200MB`
        , type: `size`
      },
      {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , bytes: 284541583
        , cost: 75
        , image: `assets/imgs/antelope.jpg`
        , name: 'Medium'
        , price: 0.35
        , size: `265MB`
        , type: `size`
      },
      {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , bytes: 322122547
        , cost: 100
        , image: `assets/imgs/buffalo.jpg`
        , name: `Big`
        , price: 0.30
        , size: `300MB`
        , type: `size`
      },
      {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , bytes: 536870912
        , cost: 200
        , image: `assets/imgs/rhino.png`
        , name: `Mega`
        , price: 0.25
        , size: `500MB`
        , type: `size`
      },
      {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , bytes: 1073741824
        , cost: 500
        , image: `assets/imgs/jumbo.png`
        , name: `Jumbo`
        , price: 0.20
        , size: `1GB`
        , type: `size`
      },
      {
        brcolor: `#2ec95c`
        , bgcolor: `#2ec95c`
        , bytes: 2684354560
        , cost: 1000
        , image: `assets/imgs/whale.png`
        , name: `Gagantua`
        , price: 0.15
        , size: `2.5GB`
        , type: `size`
      }
    ]
  }
  chooseBundle(bundle) {
    this.viewCtrl.dismiss(bundle)
  }
  explainLP() {
    this.viewCtrl.dismiss('lp')
  }
  redeemLP() {
    this.viewCtrl.dismiss('redeem')
  }
  refresh() {
    this.user.refresh(this.user._user.accounts.local.phonenumber).subscribe(
      res=>{
        this.user.save(res.user).then(()=>{
          this.balance = this.user._user.finances.balance
          this.lp =  this.user._user.finances.lp
        })
      }
    )
  }
}