import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InternetBundles } from './internet-bundles';

@NgModule({
  declarations: [
    InternetBundles,
  ],
  imports: [
    IonicPageModule.forChild(InternetBundles),
  ],
  exports: [
    InternetBundles
  ]
})
export class InternetBundlesPageModule {}
