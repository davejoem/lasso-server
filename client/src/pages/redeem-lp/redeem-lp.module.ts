import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedeemLP } from './redeem-lp';

@NgModule({
  declarations: [
    RedeemLP,
  ],
  imports: [
    IonicPageModule.forChild(RedeemLP),
  ],
  exports: [
    RedeemLP
  ]
})
export class RedeemLPModule {}
