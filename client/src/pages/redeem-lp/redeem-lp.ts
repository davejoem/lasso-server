import { Component, OnInit } from '@angular/core'
import { IonicPage, ViewController } from 'ionic-angular'
import { User } from '../../providers/providers'

@IonicPage()
@Component({
  selector: 'redeem-lp',
  templateUrl: 'redeem-lp.html',
})
export class RedeemLP implements OnInit {
  public lp: number
  
  constructor(
    private user: User
    , public viewCtrl: ViewController
  ) {}
  
  cancel() {
    this.viewCtrl.dismiss('cancel')
  }
  ngOnInit() {
    this.refresh()
  }
  redeem() {
    this.viewCtrl.dismiss('redeem')
  }
  refresh() {
    this.user.refresh(this.user._user.accounts.local.phonenumber).subscribe(
      res=>{
        this.user.save(res.user).then(()=>{
          this.lp =  res.user.finances.lp
        })
      }
    )
  }
}
