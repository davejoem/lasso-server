import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { IonicPageModule } from 'ionic-angular'
import { TransactionCode } from './transactioncode'

@NgModule({
  declarations: [
    TransactionCode,
  ],
  imports: [
    IonicPageModule.forChild(TransactionCode)
    , ReactiveFormsModule
  ],
  exports: [
    TransactionCode
  ]
})
export class TransactionCodeModule {}
