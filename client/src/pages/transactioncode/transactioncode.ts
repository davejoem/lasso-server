import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'transactioncode',
  templateUrl: 'transactioncode.html',
})
export class TransactionCode implements OnInit {
  public error: string
  public message: string
  public placeholder: string
  public subtitle: string
  public title: string
  private transactioncode: FormGroup
  constructor(
    private fb: FormBuilder
    , private navParams: NavParams
    , private viewCtrl: ViewController
  ) {
    this.error = ``
    this.message = ``
  }
  ngOnInit() {
    this.transactioncode = this.fb.group({
      transactioncode: ['', [ Validators.required, Validators.minLength(10), Validators.maxLength(10) ]]
    });
  }
  ionViewWillEnter() {
    this.error = this.navParams.get(`error`)
    this.message = this.navParams.get(`message`)
    document.getElementById('message').innerHTML = this.message
    this.placeholder = this.navParams.get(`placeholder`)
    this.subtitle = this.navParams.get(`subtitle`)
    this.title = this.navParams.get(`title`)
  }
  transact() {
    this.viewCtrl.dismiss(this.transactioncode.value)
  }
  cancel() {
    this.viewCtrl.dismiss('cancelled')
  }
}
