import { NgModule } from '@angular/core'
import { Tutorial } from './tutorial'
import { IonicPageModule } from 'ionic-angular'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [Tutorial],
  imports: [
    IonicPageModule.forChild(Tutorial)
    , TranslateModule.forChild()
  ]
  , exports: [
    Tutorial
  ]
})
export class TutorialModule { }
