import { Component, ViewChild } from '@angular/core';
import { AlertController, IonicPage, LoadingController, MenuController, NavController, Platform, Slides } from 'ionic-angular';

import { User } from '../../providers/user';

import { TranslateService } from '@ngx-translate/core';



export interface Slide {
  title: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'tutorial',
  templateUrl: 'tutorial.html'
})
export class Tutorial {
  @ViewChild(Slides) slider: Slides;
  activeindex: number;
  slides: Slide[];
  private signupErrorString: string;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public navCtrl: NavController,
    public platform: Platform,
    private translateService: TranslateService,
    private user: User
  ) {
    this.activeindex = 0;
  }
  ngOnInit() {
    this.translateService.get([
      "PHONE_NUMBER_DESCRIPTION",
      "SIGNUP_ERROR",
      "TUTORIAL_SLIDE1_TITLE",
      "TUTORIAL_SLIDE1_DESCRIPTION",
      "TUTORIAL_SLIDE2_TITLE",
      "TUTORIAL_SLIDE2_DESCRIPTION",
      "TUTORIAL_SLIDE3_TITLE",
      "TUTORIAL_SLIDE3_DESCRIPTION",
    ]).subscribe(values=>{
      this.signupErrorString = values.SIGNUP_ERROR;
      this.slides = [
        { 
          title: values.TUTORIAL_SLIDE1_TITLE,
          description: values.TUTORIAL_SLIDE1_DESCRIPTION,
          image: 'assets/imgs/ica-slidebox-img-1.png',
        },
        {
          title: values.TUTORIAL_SLIDE3_TITLE,
          description: values.TUTORIAL_SLIDE3_DESCRIPTION,
          image: 'assets/imgs/ica-slidebox-img-3.png',
        }
      ]
      if (!this.user._user) {
        this.slides.splice(1, 0, {
          title: values.TUTORIAL_SLIDE2_TITLE,
          description: values.TUTORIAL_SLIDE2_DESCRIPTION,
          image: 'assets/imgs/ica-slidebox-img-2.png',
        })
      }
    })
  }
  
  download() {
    if (this.platform.is('android')) {
      console.log("Downloading for android.")
    }
    if (!this.platform.is('mobile')) {
      console.log("Downloading for desktop.")
    }
  }
  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
    this.slider.enableKeyboardControl(true);
  }
  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
  
  nextSlide() {
    this.slider.slideNext()
    this.activeindex = this.slider._activeIndex    
  }
  
  onSlideChangeStart(slider) {
    
  }
  
  prevSlide() {
    this.slider.slidePrev()
    this.activeindex = this.slider._activeIndex
  }
  
  sign() {
    let prompt = this.alertCtrl.create({
      title: `Sign Up`,
      message: `Enter your phone number. We will send you an activation code.`,
      inputs: [
        {
          name: `phonenumber`,
          placeholder: `Phone Number E.g. 07..`
        },
      ],
      buttons: [
        {
          text: `Cancel`,
          role: 'cancel',
          handler: ()=>{
            // prompt.dismiss()
          }
        },
        {
          text: 'Sign Up',
          handler: data => {
            let loader = this.loadingCtrl.create({
              content: "Please wait..."
            })
            loader.present().then(()=>{
            this.user.signUp(data).subscribe(resp => {
              loader.dismiss().then(()=>{
                let alert = this.alertCtrl.create({
                  title: `Sign Up`,
                  message: `Enter the activation code you recieved.`,
                  inputs:[
                    {
                      name: `activationcode`,
                      placeholder: `Activation Code`
                    }
                  ],
                  buttons: [
                    {
                      text: `Cancel`,
                      handler: () => alert.dismiss()
                      
                    },
                    {
                      text: `Activate`,
                      handler: code => {
loader.present().then(()=>{                       this.user.activate({
                          code: code,
                          user: this.user._user
                        }).subscribe(resp => {
                          loader.dismiss().then(()=>{this.nextSlide()})
                        }, (err) => {
                          loader.dismiss().then(()=>{
                            this.alertCtrl.create({
                              title: `Sign Up Error`
                              , message: this.signupErrorString
                            }).present()
                          })
                        })
                      }
                    }
                  ],
                  enableBackdropDismiss: false
                })
                alert.present()
              })
            }, (err) => {
              loader.dismiss().then(()=>{
                this.alertCtrl.create({
                  title: `Sign Up Error`,
                  message: this.signupErrorString
                }).present()
              })
            })
            })
          }
        }
      ],
      enableBackdropDismiss: false
    })
    prompt.present()
  }
  
  slideChanged(slider: Slides) {
    this.activeindex = this.slider._activeIndex
  }
  
  startApp() {
    this.navCtrl.setRoot('Home', {}, {
      animate: true,
      direction: 'forward'
    });
  }
}
}
