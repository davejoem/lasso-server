module.exports = (agent)=>{
	return class Mikrotik {
    constructor(ip,username,password, opts) {
      ip ? this.ip = ip : null
      username ? this.username = username : null
      password ? this.password = password : null
      opts? this.opts = opts : null
      this.errors = []
      this._users = {
        all: []
        , active: []
        , disabled: []
      }
      this._hosts = []
      this.conn = null
      this.connections = []
    }
    addUser(params) {
      return new Promise((reject, resolve)=>{
        process.stdout.write(`creating new speed user\n`)
        let commands = []
        commands.push('/ip/hotspot/user/add')
        commands.push(`=name=${params.mac}`)
        commands.push(`=password=${params.password}`)
        switch(params.type) {
          case `speed`:
            commands.push(`=profile=${params.profile}`)
            break
          case `size`:
            commands.push(`=profile=${params.profile ? params.profile : '4Mbps_Profile'}`)
            commands.push(`=limit-bytes-total=${params.bytes}`)
            break
        }
        process.stdout.write(JSON.stringify(commands))
        this.conn.getCommandPromise(commands).then(resolve, reject)
      })
    }
    checkUserExists(username) {
      process.stdout.write(`Checking if user ${username} exists\n`)
      return new Promise((resolve, reject)=>{
        let commands = [
          `/ip/hotspot/user/print`
          , `?name=${username}`
        ]
        process.stdout.write(`Running ${JSON.stringify(commands)}`)
        agent.router.conn.getCommandPromise(commands).then(
          user=>{
            process.stdout.write(`User check found: ${JSON.stringify(user)} ${user.length}\n`)
            user.length > 0
              ? resolve(true)
              : resolve(false)
          }
          , err=>{
            process.stdout.write(`User check errored: ${JSON.stringify(err)}\n`)
            reject(false)
          }
        )
      })
    }
    connect(ip,username,password, opts) {
      let credentials = {
        ip: ip || agent.config.router.ip || this.ip
        , username: username || agent.config.router.username || this.username
        , password: password || agent.config.router.password || this.password
      }
      return new Promise((resolve, reject)=>
        agent.mikronode.getConnection(
          credentials.ip
          , credentials.username
          , credentials.password
          , opts || this.opts || { closeOnDone : false }
        ).getConnectPromise().then(
          conn=>{
            this.conn = conn
            this.logged_in = true
            this.ip = credentials.ip
            this.username = credentials.username
            this.password = credentials.password
            this.getUsers()
            resolve(conn)
          }
          , err=>{
            reject(err)
          }
        )
      )
    }

    get connected() {
      return this.logged_in
    }
    disconnect() {
      this.conn.close()
      this.conn = null
      this.logged_in = false
    }
    err(err) {
      this.errors.push(err)
    }
    getUsers() {
      return this.conn.getCommandPromise('/ip/hotspot/user/print')
    }
    get users() {
      return this._users.all
    }
    set user(users) {
      this._users.all = users
    }
    get active_users() {
      return this._users.active
    }
    get disabled_users() {
      return this._users.disabled
    }
    get hosts() {
      return this._hosts
    }
    set hosts(hosts) {
      this._hosts = hosts
    }
    removeUser(params) {}
    updateUser(params) {
      return new Promise((resolve, reject)=>{
        this.conn.getCommandPromise([
          `/ip/hotspot/user/print`
          , `?name=${params.mac}`
        ]).then(
          users=> {
            process.stdout.write(JSON.stringify(users))
            let commands = []
            commands.push('/ip/hotspot/user/set')
            commands.push(`=numbers=${parseInt(users[0][`.id`].charAt(1))}`)
            switch(params.type) {
              case `speed`:
                commands.push(`=profile=${params.profile}`)
                break
              case `size`:
                commands.push(`=profile=${params.profile ? params.profile : '4Mbps_Profile'}`)
                commands.push(`=limit-bytes-total=${params.bytes}`)
                break
            }
            this.conn.getCommandPromise(commands).then(resolve, reject)
          }
          , reject
        )
      })
    }
  }
}